﻿using System;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;

namespace CorporateAPITests
{
    public class SystemConfigurationLoader
    {
        public System.Configuration.Configuration configuration;
        /// <summary>
        /// Constructor
        /// </summary>
        public SystemConfigurationLoader() {
            //System.Configuration.ConfigurationManager.AppSettings["ConnectionString"] = "mongodb://localhost:27017";
            System.Configuration.ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = "./app.config";
            configuration = System.Configuration.ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
            foreach (var key in configuration.AppSettings.Settings.AllKeys)
            {
                try
                {
                    System.Configuration.ConfigurationManager.AppSettings[key] = configuration.AppSettings.Settings[key].Value;
                }
                catch (Exception e)
                {

                }
            }
            Console.WriteLine("Loaded Config");
        }
    }
}
