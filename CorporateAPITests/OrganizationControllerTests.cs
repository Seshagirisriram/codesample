﻿using CorporateAPI;
using CorporateAPI.BL;
using CorporateAPI.Controllers;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using CorporateAPI.Models.Response;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;
using CorporateAPI.DatabaseService;
using Moq;

namespace CorporateAPITests
{
    public class OrganizationControllerTests : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;

        Authorize authorize = null;
        private readonly OrganizationController _organizationController;
        private Mock<IMongoDBService<OrganizationModel>> organizationRepository;
        public OrganizationControllerTests(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
            authorize = new Authorize(fixture);
            _organizationController = new OrganizationController();
            organizationRepository = new Mock<IMongoDBService<OrganizationModel>>();
        }
        
        /*
        [Fact]
        public async void AddApiPathTestWithoutAuthentication() {
            var request = "/api/organization/add";
            OrganizationRequestModel SyncModel = new OrganizationRequestModel();
            SyncModel.Name = "SampleName123";
            SyncModel.BussinessContact = "32123123";
            SyncModel.TechnicalContact = "213123";
            SyncModel.Address = "asdasd3WEF";
            var response = await HttpClientHelper.Post(Client, request, SyncModel, null);
            HttpStatusCode StatusCode = (HttpStatusCode)response
               .GetType()
               .GetProperty("StatusCode")
               .GetValue(response, null);
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        */
        /*
        [Fact]
        public async void GetOrganizationApiPathTestWithOutAuthentication()
        {
            var request = "/api/organization/get/5";
            var response = await HttpClientHelper.Get(Client, request, null);
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        
        [Fact]
        public void AddOrganizationContollerResponseCodeValidation() {
            OrganizationController organizationController = new OrganizationController();
            OrganizationRequestModel requestModel = new CorporateAPI.Models.Request.OrganizationRequestModel()
            {
                Address = "Test Address",
                BussinessContact = "Bussiness Contact",
                TechnicalContact = "Technical Contact",
            };
            IActionResult result = organizationController.AddOrganization(requestModel);
            HttpStatusCode StatusCode = (HttpStatusCode)result
                .GetType()
                .GetProperty("StatusCode")
                .GetValue(result, null);
            Assert.Equal(HttpStatusCode.Created, StatusCode);
        }

        [Fact]
        public void AddOrganizationContollerResponseValidation()
        {
            OrganizationController organizationController = new OrganizationController();
            OrganizationRequestModel requestModel = new CorporateAPI.Models.Request.OrganizationRequestModel()
            {
                Address = "Test Address",
                BussinessContact = "Bussiness Contact",
                TechnicalContact = "Technical Contact",
            };
            IActionResult result = organizationController.AddOrganization(requestModel);
            CreatedResult createdResult = (CreatedResult)result;
            BaseResponseModel responseModel = (BaseResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0501", message.Code);
            Assert.Equal("Organization Added Successfully", message.Text);
            
        }
       
        [Fact]
        public void UpdateOrganizationContollerResponseCodeValidation()
        {
            OrganizationModel organizationModel = new OrganizationBL(organizationRepository.Object).GetOrganization(8);
            OrganizationController organizationController = new OrganizationController();
            OrganizationRequestModel requestModel = new CorporateAPI.Models.Request.OrganizationRequestModel()
            {
                OrganizationId = organizationModel.OrganizationId,
                Address = "Test Address",
                BussinessContact = "Bussiness Contact",
                TechnicalContact = "Technical Contact",
            };
            IActionResult result = organizationController.UpdateOrganization(requestModel);
            HttpStatusCode StatusCode = (HttpStatusCode)result
                .GetType()
                .GetProperty("StatusCode")
                .GetValue(result, null);
            Assert.Equal(HttpStatusCode.OK, StatusCode);
        }
        [Fact]
        public void UpdateOrganizationContollerResponseValidation()
        {
            OrganizationModel organizationModel = new OrganizationBL(organizationRepository.Object).GetOrganization(8);
            OrganizationController organizationController = new OrganizationController();
            OrganizationRequestModel requestModel = new CorporateAPI.Models.Request.OrganizationRequestModel()
            {
                OrganizationId=organizationModel.OrganizationId,
                Address = "Test Address",
                BussinessContact = "Bussiness Contact",
                TechnicalContact = "Technical Contact",
            };
            IActionResult result = organizationController.UpdateOrganization(requestModel);
            OkObjectResult createdResult = (OkObjectResult)result;
            BaseResponseModel responseModel = (BaseResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0401", message.Code);
            Assert.Equal("Organization updated successfully", message.Text);
        }
       
        [Fact]
        public void UpdateOrganizationContollerResponseTestWithInvalidOrganizationId()
        {
            OrganizationController organizationController = new OrganizationController();
            OrganizationRequestModel requestModel = new CorporateAPI.Models.Request.OrganizationRequestModel()
            {
                Address = "Test Address",
                BussinessContact = "Bussiness Contact",
                TechnicalContact = "Technical Contact",
            };
            IActionResult result = organizationController.UpdateOrganization(requestModel);
            OkObjectResult createdResult = (OkObjectResult)result;
            BaseResponseModel responseModel = (BaseResponseModel)createdResult.Value;
            Assert.Equal("ERR", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-E0403", message.Code);
            Assert.Equal("Organization doen't exitsts", message.Text);
        }
        
       
        [Fact]
        public void DeactivateOrganizationContollerResponseValidation()
        {
            OrganizationController organizationController = new OrganizationController();
            OrganizationRequestModel requestModel = new CorporateAPI.Models.Request.OrganizationRequestModel()
            {
                OrganizationId = 1
            };
            IActionResult result = organizationController.DeactivateOrganization(requestModel.OrganizationId);
            OkObjectResult createdResult = (OkObjectResult)result;
            BaseResponseModel responseModel = (BaseResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0401", message.Code);
            Assert.Equal("Completed successfully", message.Text);
        }
        */
    }
}
