﻿using CorporateAPI;
using CorporateAPI.Models.Request;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CorporateAPITests
{
    public class StoreApiTests : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;

        Authorize authorize = null;

        public StoreApiTests(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
            authorize = new Authorize(fixture);
        }

        // Changes by sriram...
        /*
        [Fact]
        public async Task AddStoreApiPathTest()
        {

            String authToken = await authorize.GetToken();
            var request = "/api/store/add";
            StoreRequestModel SyncModel = new StoreRequestModel();
            SyncModel.Name = "ORG-12 S-0805 1811";
            SyncModel.StoreClientId = new Guid().ToString();
            SyncModel.AllowedPeople = 15;
            SyncModel.OrganizationId = 12;
            var response = await HttpClientHelper.Post(Client, request, SyncModel, authToken);
            response.EnsureSuccessStatusCode();
            //Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
  */
        /*
        [Fact]
        public async Task AddStoreApiPathTestWithOutAuthentication()
        {
            var request = "/api/store/add";
            StoreRequestModel SyncModel = new StoreRequestModel();
            SyncModel.StoreClientId = new Guid().ToString();
            SyncModel.AllowedPeople = 15;
            var response = await HttpClientHelper.Post(Client, request, SyncModel, null);
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        */ 
    }
}
