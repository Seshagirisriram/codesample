﻿ using CorporateAPI;
using CorporateAPI.BL;
using CorporateAPI.Controllers;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using CorporateAPI.Models.Response;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using CorporateAPI.DatabaseService;
using Moq;


namespace CorporateAPITests
{
    public class StoreControllerTests : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;

        Authorize authorize = null;
        private Mock<IMongoDBService<StoreModel>> storeRepository;
        public StoreControllerTests(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
            authorize = new Authorize(fixture);
            storeRepository = new Mock<IMongoDBService<StoreModel>>();
        }

        /*
       [Fact]
       public async void AddStoreControllerBasicTest() {
           StoreController storeController = new StoreController();
           StoreRequestModel RequestModel = new StoreRequestModel() { StoreClientId = new Guid().ToString(), Name = "Test Name", AllowedPeople = 15 };
           IActionResult result= storeController.ProcessAddStore(RequestModel);
           HttpStatusCode StatusCode= (HttpStatusCode)result
               .GetType()
               .GetProperty("StatusCode")
               .GetValue(result, null);
           Assert.Equal(HttpStatusCode.OK, StatusCode);
       }


       [Fact]
       public async void AddStoreControllerBasicResultContent()
       {
           StoreController storeController = new StoreController();
           StoreRequestModel RequestModel = new StoreRequestModel() { StoreClientId = new Guid().ToString(), Name = "Test Store Cogniphi", AllowedPeople = 15,OrganizationId=13 };
           IActionResult result = storeController.ProcessAddStore(RequestModel);
           CreatedResult createdResult = (CreatedResult)result;
           StoreResponseModel responseModel = (StoreResponseModel)createdResult.Value;
           Assert.Equal("OK", responseModel.ResultCode);
           CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
           Assert.Equal("ARCA-S0401", message.Code);
           Assert.Equal("Store added successfully", message.Text);

       }
       */
        /*
        [Fact]
        public async void UpdateStoreControllerBasicTest()
        {
            StoreModel storeModel = new StoreBL(storeRepository.Object).GetStore("abcde");
            StoreController storeController = new StoreController();
            StoreRequestModel RequestModel = new StoreRequestModel() { StoreClientId = storeModel.StoreClientId, Name = "Test Name c1", AllowedPeople = 100 };
            IActionResult result = storeController.ProcessUpdateStore(RequestModel);
            HttpStatusCode StatusCode = (HttpStatusCode)result
                .GetType()
                .GetProperty("StatusCode")
                .GetValue(result, null);
            Assert.Equal(HttpStatusCode.OK, StatusCode);
        }
        */
        /*
        [Fact]
        public async void UpdateStoreControllerResponseTest()
        {
            StoreModel storeModel = new StoreBL(storeRepository.Object).GetStore("abcde");
            StoreController storeController = new StoreController();
            StoreRequestModel RequestModel = new StoreRequestModel() { StoreClientId = storeModel.StoreClientId, Name = "Test Name c1", AllowedPeople = 100,StoreId=storeModel.StoreId };
            IActionResult result = storeController.ProcessUpdateStore(RequestModel);
            OkObjectResult createdResult = (OkObjectResult)result;
            StoreResponseModel responseModel = (StoreResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0401", message.Code);
            Assert.Equal("Store updated successfully", message.Text);
        }
        */

        /*
        [Fact]
        public async void UpdateStoreControllerResponseTestWithInvalidStoreClientId()
        {
            StoreController storeController = new StoreController();
            StoreRequestModel RequestModel = new StoreRequestModel() { StoreClientId = "abcdefg", Name = "Test Name c1", AllowedPeople = 100 };
            IActionResult result = storeController.ProcessUpdateStore(RequestModel);
            OkObjectResult createdResult = (OkObjectResult)result;
            StoreResponseModel responseModel = (StoreResponseModel)createdResult.Value;
            Assert.Equal("ERR", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-E0403", message.Code);
            Assert.Equal("Store doesn't exists", message.Text);
        }
         [Fact]
        public void ActivateStoreControllerValidResponseTests()
        {
            StoreModel storeModel = new StoreModel()
            {
                StoreId = 8
            };
            StoreController storeController = new StoreController();
            IActionResult result = storeController.ActivateStore(storeModel.StoreId);
            OkObjectResult createdResult = (OkObjectResult)result;
            StoreResponseModel responseModel = (StoreResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0401", message.Code);
            Assert.Equal("Completed successfully", message.Text);
        }
        [Fact]
        public void DeActivateStoreControllerValidResponseTests()
        {
            StoreModel storeModel = new StoreModel()
            {
                StoreId = 8
            };
            StoreController storeController = new StoreController();
            IActionResult result = storeController.DeactivateStore(storeModel.StoreId);
            OkObjectResult createdResult = (OkObjectResult)result;
            StoreResponseModel responseModel = (StoreResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0401", message.Code);
            Assert.Equal("Completed successfully", message.Text);
        }
        

        [Fact]
        public void ValidTimeZone()
        {
            
            StoreRequestModel requestModel = new StoreRequestModel() { TimeZone = "+05:30" };
            Assert.Equal("+05:30", requestModel.TimeZone);
        }

        [Fact]
        public void ValidAllowedPeople()
        {
            StoreRequestModel requestModel = new StoreRequestModel() { AllowedPeople=8};
            Assert.Equal(8, requestModel.AllowedPeople);
        }
        */

    }
}
