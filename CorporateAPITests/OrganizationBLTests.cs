﻿using CorporateAPI;
using CorporateAPI.BL;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using CorporateAPI.DatabaseService;
using Moq;
using System.Net;
using System.Net.Http;

namespace CorporateAPITests
{
    public class OrganizationBLTests : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;
        Authorize authorize = null;
        private Mock<IMongoDBService<OrganizationModel>> organizationRepository;

        public OrganizationBLTests(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
            authorize = new Authorize(fixture);
            organizationRepository = new Mock<IMongoDBService<OrganizationModel>>();
        }
        /*
        [Fact]
        public void AddOrganizationWithValidInformationsTest() {
            OrganizationBL organizationBL = new OrganizationBL(organizationRepository.Object);
            OrganizationRequestModel requestModel = new CorporateAPI.Models.Request.OrganizationRequestModel()
            {
                Address = "Test Address",
                BussinessContact = "Bussiness Contact",
                TechnicalContact = "Technical Contact",
            };
            OrganizationModel organizationModel= organizationBL.AddOrganization(requestModel).Result;

            Assert.NotNull(organizationModel);
            OrganizationModel currentAdddedOrganization = organizationBL.GetOrganization(organizationModel.OrganizationId);
            Assert.Equal(organizationModel.OrganizationId, currentAdddedOrganization.OrganizationId); 
            Assert.Equal(requestModel.Address, currentAdddedOrganization.Address);
            Assert.Equal(requestModel.BussinessContact, currentAdddedOrganization.BussinessContact);
            Assert.Equal(requestModel.TechnicalContact, currentAdddedOrganization.TechnicalContact);
        }
        [Fact]
        public void UpdateOrganizationWithValidInformationsTest()
        {
            OrganizationBL organizationBL = new OrganizationBL(organizationRepository.Object);
            OrganizationModel requestModel = new CorporateAPI.Models.OrganizationModel()
            {
                OrganizationId=8,
                Address = "Test Address",
                BussinessContact = "Bussiness Contact",
                TechnicalContact = "Technical Contact",
            };
            bool isUpdated  = organizationBL.UpdateOrganization(requestModel).Result;
            Assert.NotNull(requestModel);
            Assert.True(isUpdated); 
            OrganizationModel currentAdddedOrganization = organizationBL.GetOrganization(requestModel.OrganizationId);
            Assert.Equal(requestModel.OrganizationId, currentAdddedOrganization.OrganizationId);
            Assert.Equal(requestModel.Address, currentAdddedOrganization.Address);
            Assert.Equal(requestModel.BussinessContact, currentAdddedOrganization.BussinessContact);
            Assert.Equal(requestModel.TechnicalContact, currentAdddedOrganization.TechnicalContact);
          
        }
        [Fact]
        public void DeactivateOrganizationWithValidInformationsTest()
        {
            OrganizationBL organizationBL = new OrganizationBL(organizationRepository.Object);
            OrganizationModel requestModel = new CorporateAPI.Models.OrganizationModel()
            {
                OrganizationId=8,
                IsActive=false,
            };
            bool isUpdated = organizationBL.UpdateOrganization(requestModel).Result;
            Assert.NotNull(requestModel);

             OrganizationModel currentAdddedOrganization = organizationBL.GetOrganization(requestModel.OrganizationId);
            Assert.Equal(requestModel.OrganizationId, currentAdddedOrganization.OrganizationId);
            Assert.Equal(requestModel.IsActive, currentAdddedOrganization.IsActive);
        }
        
         */


    }
}
