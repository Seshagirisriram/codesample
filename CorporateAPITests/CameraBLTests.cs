﻿using CorporateAPI.BL;
using CorporateAPI.Dto;
using CorporateAPI.Exceptions;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using CorporateAPI.DatabaseService;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using CorporateAPI;
using System.Net;
using System.Net.Http;

namespace CorporateAPITests
{
    public class CameraBLTests : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;
        Authorize authorize = null;
        private Mock<IMongoDBService<StoreModel>> storeRepository;
        private Mock<IMongoDBService<CameraModel>> cameraRepository;

        public CameraBLTests(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
            authorize = new Authorize(fixture);
            cameraRepository = new Mock<IMongoDBService<CameraModel>>();
            storeRepository = new Mock<IMongoDBService<StoreModel>>();
        }
        [Fact]
        public void GetCameraBasedOnInvalidStoreId() {
            CameraBL cameraBL = new CameraBL(cameraRepository.Object,storeRepository.Object);
            List<CameraModel> Cameras= cameraBL.GetCameras(8);
            Assert.Empty(Cameras);
        }

        /*
        [Fact]
        public void AddCameraBasedOnValidStoreId() {
            CameraBL cameraBl = new CameraBL(cameraRepository.Object, storeRepository.Object);
            CameraModel cameraModel= cameraBl.AddCamera(new CameraRequestModel() { 
                CameraIp = "192.168.2.50", Password = "test", Protocol="rtsp",StoreClientId="abcde",UserName="test" }).Result;

            Assert.Equal("192.168.2.50", cameraModel.CameraIp);

        }
        */ 
        [Fact]
        public void AddCameraBasedOnInValidStoreId()
        {
            CameraBL cameraBl = new CameraBL(cameraRepository.Object, storeRepository.Object);
            Assert.Throws<AggregateException>(() => cameraBl.AddCamera(new CameraRequestModel()
            {
                CameraIp = "192.168.2.50",
                Password = "test",
                Protocol = "rtsp",
                StoreClientId = "abcdedasd",
                UserName = "test"
            }).Result);

        }


    }
}
