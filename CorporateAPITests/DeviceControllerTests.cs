﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using CorporateAPI.Controllers;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using CorporateAPI.Models.Response;
using Moq;
using CorporateAPI.DatabaseService;
using System.Net;
using System.Net.Http;
using CorporateAPI;

namespace CorporateAPITests
{
    public class DeviceControllerTests : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;

        Authorize authorize = null;
        private Mock<IMongoDBService<DeviceModel>> deviceRepository;
        public DeviceControllerTests(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
            authorize = new Authorize(fixture);
            deviceRepository = new Mock<IMongoDBService<DeviceModel>>();
        }

        /*
        [Fact]
        public void AddDeviceBasicControllerTests() {

            DeviceRequestModel requestModel = new DeviceRequestModel() { 
                IpAddress="10.732.24.28",
                MacAddress="MacAddress",
                SerialNumber="ASSDS",
                Type="Linux",
                StoreId = 8
            };

            DeviceController deviceController = new DeviceController();
            IActionResult result= deviceController.ProcessAddDeviceRequest(requestModel);
            HttpStatusCode StatusCode = (HttpStatusCode)result
                .GetType()
                .GetProperty("StatusCode")
                .GetValue(result, null);
            Assert.Equal(HttpStatusCode.OK, StatusCode);
        }
        */
        /*
        [Fact]
        public void AddDeviceControllerValidResponseTests()
        {

            DeviceRequestModel requestModel = new DeviceRequestModel()
            {
                IpAddress = "10.732.24.29",
                MacAddress = "MacAddress",
               // SerialNumber = "ASSDSF",
                Type = "Linux",
               StoreId = 8
            };

            DeviceController deviceController = new DeviceController();
            IActionResult result = deviceController.ProcessAddDeviceRequest(requestModel);
            CreatedResult createdResult = (CreatedResult)result;
            DeviceResponseModel responseModel = (DeviceResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0404", message.Code);
            Assert.Equal("Device added successfully", message.Text);
        }
        
        [Fact]
        public async void UpdateDeviceControllerBasicTest()
        {
            DeviceController deviceController = new DeviceController();
            DeviceRequestModel requestModel = new DeviceRequestModel()
            {
                IpAddress = "10.732.24.28",
                MacAddress = "MacAddress",
                SerialNumber = "ASSDS",
                Type = "Linux",
                DeviceId = 17,
                StoreId=8
            };
            IActionResult result = deviceController.ProcessUpdateDeviceRequest(requestModel);
            HttpStatusCode StatusCode = (HttpStatusCode)result
                .GetType()
                .GetProperty("StatusCode")
                .GetValue(result, null);
            Assert.Equal(HttpStatusCode.OK, StatusCode);
        }

        [Fact]
        public void UpdateDeviceControllerValidResponseTests()
        {

            DeviceRequestModel requestModel = new DeviceRequestModel()
            {
                IpAddress = "10.732.24.28",
                MacAddress = "MacAddress",
                SerialNumber = "ASSDS",
                Type = "Linux",
                DeviceId = 17,
                StoreId = 8

            };
           
            DeviceController deviceController = new DeviceController();
            IActionResult result = deviceController.UpdateDevice(requestModel);
            OkObjectResult createdResult = (OkObjectResult)result;
            DeviceResponseModel responseModel = (DeviceResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0404", message.Code);
            Assert.Equal("Device updated successfully", message.Text);
        }
        
        [Fact]
        public void ActivateDeviceControllerValidResponseTests()
       {
            DeviceModel deviceModel = new DeviceModel()
            {
                DeviceId = 17
            };

            DeviceController deviceController = new DeviceController();
            IActionResult result = deviceController.ActivateDevice(deviceModel.DeviceId);
            OkObjectResult createdResult = (OkObjectResult)result;
            DeviceResponseModel responseModel = (DeviceResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0401", message.Code);
            Assert.Equal("Completed successfully", message.Text);
        }
        [Fact]
        public void DeActivateDeviceControllerValidResponseTests()
        {

            DeviceModel deviceModel = new DeviceModel()
            {
                DeviceId = 17
            };

            DeviceController deviceController = new DeviceController();
            IActionResult result = deviceController.DeactivateDevice(deviceModel.DeviceId);
            OkObjectResult createdResult = (OkObjectResult)result;
            DeviceResponseModel responseModel = (DeviceResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0401", message.Code);
            Assert.Equal("Completed successfully", message.Text);
        }
        [Fact]
        public async void GetDeviceApiPathTestWithOutAuthentication()
        {
            var request = "/api/store/device/get/17";
            var response = await HttpClientHelper.Get(Client, request, null);
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        [Fact]
        public void ValidIPAddress()
        {
            DeviceRequestModel requestModel = new DeviceRequestModel() { IpAddress = "10.27.46.1" };
            Assert.Equal("10.27.46.1", requestModel.IpAddress);
        }
        */

    }
}
