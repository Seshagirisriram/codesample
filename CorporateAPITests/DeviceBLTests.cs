﻿using CorporateAPI;
using CorporateAPI.BL;
using CorporateAPI.BL;
using CorporateAPI.Dto;
using CorporateAPI.Exceptions;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using CorporateAPI.DatabaseService;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using System.Net;
using System.Net.Http;

namespace CorporateAPITests
{
    public class DeviceBLTests : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;
        Authorize authorize = null;
        private Mock<IMongoDBService<DeviceModel>> deviceRepository;
        private Mock<IMongoDBService<StoreModel>> storeRepository;

        public DeviceBLTests(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
            authorize = new Authorize(fixture);
            deviceRepository = new Mock<IMongoDBService<DeviceModel>>();
            storeRepository = new Mock<IMongoDBService<StoreModel>>();
        }
        /*
        [Fact]
        public void AddDeviceBasicTest() {
            DeviceBL deviceBl = new DeviceBL(storeRepository.Object,deviceRepository.Object);
            deviceRepository = new Mock<IMongoDBService<DeviceModel>>();
            bool IsDeviceAdded = deviceBl.AddDevice(new CorporateAPI.Models.DeviceModel() {
                StoreId = 1,
                IpAddress="192.168.2.50",
                MacAddress="MAC Address 1",
                SerialNumber="Serial Number 1",
                Type="Windows"
            }).Result;
            Assert.True(IsDeviceAdded);
        }

        [Fact]
        public async void DeactivateDeviceWithValidInformationsTest()
        {
            DeviceBL deviceBl = new DeviceBL(storeRepository.Object, deviceRepository.Object);
            deviceRepository = new Mock<IMongoDBService<DeviceModel>>();
            DeviceModel requestModel = new CorporateAPI.Models.DeviceModel()
            {
                DeviceId = 14,
                IsActive = false,
            };
            bool isUpdated = deviceBl.UpdateDevice(requestModel).Result;
            
            Assert.NotNull(requestModel);
            Assert.True(isUpdated); 
            DeviceModel currentAddedDevice=  deviceBl.GetDevice(requestModel.DeviceId).Result;
            Assert.Equal(requestModel.DeviceId, currentAddedDevice.DeviceId);
            Assert.Equal(requestModel.IsActive, currentAddedDevice.IsActive);
        }

        [Fact]
        public async void ActivateDeviceWithValidInformationsTest()
        {
            DeviceBL deviceBl = new DeviceBL(storeRepository.Object, deviceRepository.Object);
            deviceRepository = new Mock<IMongoDBService<DeviceModel>>();
            DeviceModel requestModel = new CorporateAPI.Models.DeviceModel()
            {
                DeviceId = 17,
                IsActive = true,
            };
            bool isUpdated = deviceBl.UpdateDevice(requestModel).Result;
            Assert.NotNull(requestModel);
            Assert.True(isUpdated); 
            DeviceModel currentAdddedDevice = deviceBl.GetDevice(requestModel.DeviceId).Result;
            Assert.Equal(requestModel.DeviceId, currentAdddedDevice.DeviceId);
            Assert.Equal(requestModel.IsActive, currentAdddedDevice.IsActive);
        }
                    */

    }
}
