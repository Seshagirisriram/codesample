﻿using CorporateAPI;
using CorporateAPI.Models.Request;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CorporateAPITests
{
    public class DeviceAPITests : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;

        Authorize authorize = null;

        public DeviceAPITests(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
            authorize = new Authorize(fixture);
        }

        /*
        [Fact]
        public async Task AddDeviceApiPathTest()
        {
            String authToken = await authorize.GetToken();
            var request = "/api/store/Device/add";
            DeviceRequestModel SyncModel = new DeviceRequestModel();
            SyncModel.StoreId = 12;
            SyncModel.Type = "Raspberry Pi 4";
            SyncModel.IpAddress = "192.168.2.62";
            SyncModel.MacAddress = "ABCDEF";
            SyncModel.SerialNumber = "00FFAA";            
            var response = await HttpClientHelper.Post(Client, request, SyncModel, authToken);
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
    }
     */ 
        
        [Fact]
        public async Task AddDeviceApiPathTestWithOutAuthentication()
        {
            var request = "/api/store/device/add";
            StoreRequestModel SyncModel = new StoreRequestModel();
            SyncModel.StoreClientId = new Guid().ToString();
            SyncModel.AllowedPeople = 15;
            var response = await HttpClientHelper.Post(Client, request, SyncModel, null);
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

    }
}
