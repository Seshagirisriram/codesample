﻿using CorporateAPI;
using CorporateAPI.BL;
using CorporateAPI.Dto;
using CorporateAPI.Exceptions;
using CorporateAPI.Models;
using CorporateAPI.DatabaseService;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using System.Net;
using System.Net.Http;

namespace CorporateAPITests
{

    public class StoreBLTests : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;
        Authorize authorize = null;
        private Mock<IMongoDBService<StoreModel>> storeRepository;
        public StoreBLTests(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
            authorize = new Authorize(fixture);
            storeRepository = new Mock<IMongoDBService<StoreModel>>();
        }
      
        /*
        [Fact]
        public void AddStoreBasicTests() {
          
            StoreBL storeBL = new StoreBL(storeRepository.Object);
            CorporateAPI.Models.StoreModel storeModel = null;
            storeRepository = new Mock<IMongoDBService<StoreModel>>();
            try
            {
                storeModel = storeBL.GetStore("abcd");
            }
            catch (Exception e) { 
            }
            StoreModel addedStore = null;
          
            if (storeModel == null) {
                addedStore = storeBL.AddStore(new CorporateAPI.Models.StoreModel()
                {
                    StoreClientId = "abcd"
                }).Result;

            }
            Assert.NotNull(addedStore);
        }

        [Fact]
        public void GetStoreFromValidStoreClientId()
        {
            StoreBL storeBL = new StoreBL(storeRepository.Object);
            CorporateAPI.Models.StoreModel storeModel = storeBL.GetStore("test_sriram_code");
            Assert.Equal("test_sriram_code", storeModel.StoreClientId);

        }

        [Fact]
        public void GetStoreFromInvalidStoreClientId()
        {
            StoreBL storeBL = new StoreBL(storeRepository.Object);
            CorporateAPI.Models.StoreModel storeModel = storeBL.GetStore("k");
            Assert.Equal(null,storeModel);
        }

        [Fact]
        public void GetStoreDetailsFromInvalidStoreClientId()
        {
            StoreBL storeBL = new StoreBL(storeRepository.Object);
            StoreDetailsDto storeDetails = storeBL.GetStoreDetails("123");
            Assert.Equal(null, storeDetails);
        }
        */
    }
}
