﻿using CorporateAPI;
using CorporateAPI.BL;
using CorporateAPI.Dto;
using CorporateAPI.Exceptions;
using CorporateAPI.Models;
using CorporateAPI.DatabaseService;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using System.Net;
using System.Net.Http;

namespace CorporateAPITests
{
    public class ZonesBLTests : IClassFixture<TestFixture<Startup>>
    {
        private Mock<IMongoDBService<ZoneModel>> zoneRepository;

        private HttpClient Client;
        Authorize authorize = null;
  
        public ZonesBLTests(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
            authorize = new Authorize(fixture);
            zoneRepository = new Mock<IMongoDBService<ZoneModel>>();
        }
        /*
        [Fact]
        public void AddZonesBasedOnValidCameraId() {
            ZonesBL zoneBl = new ZonesBL(zoneRepository.Object);
            ZoneModel zoneModel = zoneBl.AddZone(new CorporateAPI.Models.Request.ZoneRequestModel()
            {
                CameraId = 1,
                InZoneCount=18,
                LayoutCoordinates="0,12,13,14",
                PremiseZoneId=121,
                PremiseZoneName="SampleName",
                SceneCoordinates="0,12,13,14",
                SystemZoneId=12,
                SystemZoneName="SystemZoneName",
                ZoneClientId="asas-dsadsa-dasdasd",
            }).Result;
            Assert.Equal(18, zoneModel.InZoneCount);
            Assert.Equal("0,12,13,14", zoneModel.LayoutCoordinates);
            Assert.Equal(1, zoneModel.CameraId);
            Assert.Equal(121, zoneModel.PremiseZoneId);
            Assert.Equal("0,12,13,14", zoneModel.SceneCoordinates);
            Assert.Equal(12, zoneModel.SystemZoneId);
            Assert.Equal("SystemZoneName", zoneModel.SystemZoneName);
        }

        [Fact]
        public void GetZonesBasedOnInvalidCameraId() {
            ZonesBL zoneBl = new ZonesBL(zoneRepository.Object);
            List<ZoneModel> zoneModels= zoneBl.GetZones(-1);
            Assert.Empty(zoneModels);
        }


        [Fact]
        public void GetZonesBasedOnValidCameraId()
        {
            ZonesBL zoneBl = new ZonesBL(zoneRepository.Object);
            List<ZoneModel> zoneModels = zoneBl.GetZones(1);
            Assert.NotEmpty(zoneModels);
        }
        */
    }
}
