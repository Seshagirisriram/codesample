﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CorporateAPITests
{
    public static class HttpClientHelper
    {
        /// <summary>
        /// Helper function to post the data to the Server
        /// </summary>
        /// <param name="Client">Http Client Instance</param>
        /// <param name="url">Url</param>
        /// <param name="PostData">Post Data String</param>
        /// <param name="authToken">Authentication Token</param>
        /// <returns>Htto Response</returns>
        public static async Task<HttpResponseMessage> Post(HttpClient Client,String url,Object postDataObject,String authToken) {
            String PostData = JsonConvert.SerializeObject(postDataObject);
            var stringContent = new StringContent(PostData, UnicodeEncoding.UTF8, "application/json");
            if(!String.IsNullOrEmpty(authToken))
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
            var response = await Client.PostAsync(url, stringContent);
            return response;
        }


        /// <summary>
        /// Helper function to get the data from the server
        /// </summary>
        /// <param name="Client">Http Client Instance</param>
        /// <param name="url">Url</param>
        /// <param name="authToken">Authentication Token</param>
        /// <returns>Htto Response</returns>
        public static async Task<HttpResponseMessage> Get(HttpClient Client, String url, String authToken)
        {
            if (!String.IsNullOrEmpty(authToken))
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
            var response = await Client.GetAsync(url);
            return response;
        }

    }
}
