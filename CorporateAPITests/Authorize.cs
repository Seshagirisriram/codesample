﻿using CorporateAPI;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using CorporateAPI.Models.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CorporateAPITests
{
    public class Authorize :  IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;

        public Authorize(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
            SystemConfigurationLoader Loader = new SystemConfigurationLoader();
        }


        public async Task<String> GetToken() {
            var request = "/api/users/authenticate";
            AuthenticateModel AuthenticateModel = new AuthenticateModel();
            AuthenticateModel.Username = "blah";
            AuthenticateModel.Password = "blah";
            String PostData = JsonConvert.SerializeObject(AuthenticateModel);
            var stringContent = new StringContent(PostData, UnicodeEncoding.UTF8, "application/json");
            var response = await Client.PostAsync(request, stringContent);
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            AuthenticationResponseModel responseModel = JsonConvert.DeserializeObject<AuthenticationResponseModel>(responseString);
            return responseModel.AuthenticationToken;
        }
    }
}
