﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using CorporateAPI.Controllers;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using CorporateAPI.Models.Response;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net.Http;
using CorporateAPI;
using Moq;

namespace CorporateAPITests
{
    public class CameraControllerTests : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;

        Authorize authorize = null;
        public CameraControllerTests(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
            authorize = new Authorize(fixture);
           
        }
        /*
        [Fact]
        public void AddCameraControllerValidDataTest() {
            CameraRequestModel requestModel = new CameraRequestModel() { 
                CameraIp="192.168.2.50",
                CameraName="Camera Name",
                Password="****",
                Protocol="rtsp",
                StoreClientId="abcde",
                UserName="root",
                Murl="rtsp://asdasdas",
                MurlSuffix="/api/teest"
            };

            CameraController controller = new CameraController();
            IActionResult result= controller.ProcessAddCamera(requestModel);
            HttpStatusCode StatusCode = (HttpStatusCode)result
                .GetType()
                .GetProperty("StatusCode")
                .GetValue(result, null);
            Assert.Equal(HttpStatusCode.OK, StatusCode);

        }
        
        [Fact]
        public void AddCameraControllerResponseValidationTest()
        {
            CameraRequestModel requestModel = new CameraRequestModel()
            {
                CameraIp = "192.168.2.9",
                CameraName = "Camera Name Test",
                Password = "****",
                Protocol = "rtsp",
                StoreClientId = "abcde",
                UserName = "root",
                Murl = "rtsp://asdasdas",
                MurlSuffix = "/api/teest"
            };

            CameraController controller = new CameraController();
            IActionResult result = controller.ProcessAddCamera(requestModel);
            CreatedResult createdResult = (CreatedResult)result;
            CameraResponseModel responseModel = (CameraResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0407", message.Code);
            Assert.Equal("Camera added successfully", message.Text);

        }
        
        [Fact]
        public void UpdateCameraControllerValidDataTest()
        {
            CameraRequestModel requestModel = new CameraRequestModel()
            {
             //   CameraIp = "192.168.2.57",
                CameraName = "Camera Name",
                Password = "****",
                Protocol = "rtsp",
                StoreClientId = "abcde",
                UserName = "root",
                Murl = "rtsp://asdasdas",
                MurlSuffix = "/api/teest",
                CameraId=20
            };

            CameraController controller = new CameraController();
            IActionResult result = controller.ProcessUpdateCamera(requestModel);
            OkObjectResult createdResult = (OkObjectResult)result;
            CameraResponseModel responseModel = (CameraResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0401", message.Code);
            Assert.Equal("Camera updated successfully", message.Text);

        }
        [Fact]
        public void UpdateCameraControllerInValidIPTest()
        {
            CameraRequestModel requestModel = new CameraRequestModel()
            {
                CameraIp = "192.168.2.53",
                CameraName = "Camera Test New",
                Password = "****",
                Protocol = "rtsp",
                StoreClientId = "abcde",
                UserName = "root",
                Murl = "rtsp://asdasdas",
                MurlSuffix = "/api/teest",
                CameraId = 20
            };

            CameraController controller = new CameraController();
            IActionResult result = controller.ProcessUpdateCamera(requestModel);
            OkObjectResult createdResult = (OkObjectResult)result; 
            CameraResponseModel responseModel = (CameraResponseModel)createdResult.Value;
            Assert.Equal("ERR", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-E0412", message.Code);
            Assert.Equal("IP Address with the same store client id already exists", message.Text);

        }
        [Fact]
        public void AddCameraControllerInValidStoreClientIdTest()
        {
            CameraRequestModel requestModel = new CameraRequestModel()
            {
                CameraName = "Camera Name",
                Password = "****",
                Protocol = "rtsp",
                StoreClientId = "invalid",
                UserName = "root",
                Murl = "rtsp://asdasdas",
                MurlSuffix = "/api/teest",
                CameraId=20
            };

            CameraController controller = new CameraController();
            IActionResult result = controller.ProcessAddCamera(requestModel);
            OkObjectResult createdResult = (OkObjectResult)result;
            CameraResponseModel responseModel = (CameraResponseModel)createdResult.Value;
            Assert.Equal("ERR", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-E0412", message.Code);
            Assert.Equal("Store Client Id doesn't exists", message.Text);

        }
        [Fact]
        public void ActivateCameraControllerValidResponseTests()
        {
            CameraModel cameraModel = new CameraModel()
            {
                CameraId = 20
            };
            CameraController controller = new CameraController();
            IActionResult result = controller.ActivateCamera(cameraModel.CameraId);
            OkObjectResult createdResult = (OkObjectResult)result;
            CameraResponseModel responseModel = (CameraResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0401", message.Code);
            Assert.Equal("Completed successfully", message.Text);
        }
        [Fact]
        public void DeActivateCameraControllerValidResponseTests()
        {
            CameraModel cameraModel = new CameraModel()
            {
                CameraId = 20
            };
            CameraController controller = new CameraController();
            IActionResult result = controller.DeactivateCamera(cameraModel.CameraId);
            OkObjectResult createdResult = (OkObjectResult)result;
            CameraResponseModel responseModel = (CameraResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0401", message.Code);
            Assert.Equal("Completed successfully", message.Text);
        }
        [Fact]
        public async void GetCameraApiPathTestWithAuthentication()
        {
            String authToken = await authorize.GetToken();
            var request = "/api/store/camera/get/10";
            var response = await HttpClientHelper.Get(Client, request, authToken);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
        [Fact]
        public async void GetCameraApiPathTestWithOutAuthentication()
        {
            var request = "/api/store/camera/get/10";
            var response = await HttpClientHelper.Get(Client, request, null);
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        [Fact]
        public void ValidIPAddress()
        {
            CameraRequestModel requestModel = new CameraRequestModel() {CameraIp= "10.27.46.1" };
            Assert.Equal("10.27.46.1", requestModel.CameraIp);
        }
        [Fact]
        public void ZoneDownloadFileResponseTests()
        {
            ZoneModel zoneModel = new ZoneModel()
            {
                ZoneId = 20
            };
            string path = "D:\\Cogniphi\\ymlUpload\\20.yml";
            CameraController controller = new CameraController();
            IActionResult result = controller.DownloadZoneFile(zoneModel.ZoneId);
            FileStreamResult createdResult = (FileStreamResult)result;
            FileStream responseModel = (FileStream)createdResult.FileStream;
           // Assert.Equal("404", message.Code);
            Assert.Equal(path, responseModel.Name);
            //Assert.Equal("OK", responseModel.ResultCode);
          //  CorporateAPI.Models.Response.Message message = responseModel.Name;
        
        }
        [Fact]
        public void ZoneDownloadFileWithInvalidZoneId()
        {
            ZoneModel zoneModel = new ZoneModel()
            {
                ZoneId = 8
            };
            CameraController controller = new CameraController();
            IActionResult result = controller.DownloadZoneFile(zoneModel.ZoneId);
            BadRequestObjectResult createdResult = (BadRequestObjectResult)result;
            BaseResponseModel responseModel = (BaseResponseModel)createdResult.Value;
            Assert.Equal("ERR", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-E0408", message.Code);
            Assert.Equal("File Not Found", message.Text);
        }

        [Fact]
        public async void GetZoneApiPathTestWithOutAuthentication()
        {
            var request = "/api/store/camera/zone/get/16";
            var response = await HttpClientHelper.Get(Client, request, null);
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        [Fact]
        public void ActivateZoneControllerValidResponseTests()
        {
            ZoneModel zoneModel = new ZoneModel()
            {
                CameraId = 20
            };
            CameraController controller = new CameraController();
            IActionResult result = controller.ActivateZone(zoneModel.CameraId);
            OkObjectResult createdResult = (OkObjectResult)result;
            BaseResponseModel responseModel = (BaseResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0401", message.Code);
            Assert.Equal("Completed successfully", message.Text);
        }
        [Fact]
        public void DeActivateZoneControllerValidResponseTests()
        {
            ZoneModel zoneModel = new ZoneModel()
            {
                ZoneId = 20
            };
            CameraController controller = new CameraController();
            IActionResult result = controller.DeactivateZone(zoneModel.ZoneId);
            OkObjectResult createdResult = (OkObjectResult)result;
            BaseResponseModel responseModel = (BaseResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0401", message.Code);
            Assert.Equal("Completed successfully", message.Text);
        }

        [Fact]
        public void AddZoneControllerInValidDataTest()
        {
            ZoneRequestModel requestModel = new ZoneRequestModel()
            {
                CameraId=9, 
                ZoneClientId="TEA-COFFEE",
                InZoneCount=5

            };
            IFormFile file = null;
            CameraController controller = new CameraController();
            IActionResult result = controller.ProcessAddZone(requestModel,file);
            BadRequestObjectResult createdResult = (BadRequestObjectResult)result;
            BaseResponseModel responseModel = (BaseResponseModel)createdResult.Value;
            Assert.Equal("ERR", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-E0411", message.Code);
            Assert.Equal("File not found", message.Text);

        }
        [Fact]
        public void AddZoneControllerResponseValidationTest()
        {
            ZoneRequestModel requestModel = new ZoneRequestModel()
            {
                CameraId = 9,
                ZoneClientId = "TEA-COFFEE",
                InZoneCount = 5

            };
          
            var fileMock = new Mock<IFormFile>();
          
            var fileName = @"16.yml"; 
            fileMock.Setup(_ => _.FileName).Returns(fileName);
        
            var file = fileMock.Object;
            CameraController controller = new CameraController();
            IActionResult result = controller.ProcessAddZone(requestModel, file);
            CreatedResult createdResult = (CreatedResult)result;
            BaseResponseModel responseModel = (BaseResponseModel)createdResult.Value;
            Assert.Equal("OK", responseModel.ResultCode);
            CorporateAPI.Models.Response.Message message = responseModel.Messages[0];
            Assert.Equal("ARCA-S0409", message.Code);
            Assert.Equal("Zone added successfully", message.Text);

        }
        */
    }
}
