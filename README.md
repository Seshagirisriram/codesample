This is our sample GITLAB CI/CD page integrated with Corporate API. The buid pipeline 
consists of 
* Compile
* Test with MongoDB on remote Server
* Publish application to AWS

Our application web page can be found here [Corporate API Web Page.](https://seshagirisriram.gitlab.io/dotnetpublish)
 
Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)


<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by GITLAB CI,  following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest
variables:
  NUGET_PACKAGES_DIRECTORY: '.nuget'

stages:
  - build
  - test
  - deploy

build:
  image: "mcr.microsoft.com/dotnet/core/sdk:3.1"
  stage: build
  before_script:
  - 'dotnet restore --packages $NUGET_PACKAGES_DIRECTORY'
  script:
    - 'echo BUILD DIR and Project DIR : $CI_BUILDS_DIR and $CI_PROJECT_DIR && mkdir -p $CI_PROJECT_DIR/reports/html'
    - 'dotnet build --no-restore | grep -v "^  " | grep -v "Time Elapsed" | awk  -F ":" -f getbuildinfo.awk | sed "s/:[ \t]/:/g" | sort |uniq | awk -F ":" -f genanalysis.awk | tee $CI_PROJECT_DIR/reports/html/codeanalysis.html' 
    - 'mkdir -p $CI_PROJECT_DIR/published'
    - 'dotnet publish -c release --force -o  $CI_PROJECT_DIR/publishedcorpapi  -r linux-x64 --self-contained true ./CorporateAPI/CorporateAPI.csproj'
  artifacts: 
    paths: 
      - ./**/*PasswordValidator*.dll
      - ./**/*codeanalysis.*
      - ./**/published 
      - ./**/publishedcorpapi
    when: on_success
test:
  image: "mcr.microsoft.com/dotnet/core/sdk:3.1"
  stage: test
  dependencies:
    - build 
  script:
    - 'dotnet test --test-adapter-path:. --logger:"junit;LogFilePath=artifacts/junit-test-result.xml;MethodFormat=Class;FailureBodyFormat=Verbose"'
    - 'dotnet tool install --global dotnet-reportgenerator-globaltool' 
    - 'export PATH=$HOME/.dotnet/tools:$PATH' 
    - 'dotnet test /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:Exclude="[xunit*]\*" /p:CoverletOutput="./TestResults/"'
    - 'reportgenerator "-reports:**/TestResults/coverage.opencover.xml" "-targetdir:reports/html" -reporttypes:HTML;'
    - 'chmod +x $CI_PROJECT_DIR/*.sh && echo Set permissions on $CI/PROJECT_DIR/run.sh' 
    - 'bash -c $CI_PROJECT_DIR/run.sh' 
  artifacts:
    when: always
    paths:
      - ./**/*.xml
      - ./**/*reports*
    reports:
      junit:
        - ./**/*junit-test-result.xml
pages:
  image: alpine:latest
  stage: deploy
  dependencies:
    - build 
    - test 
  before_script:
  - 'mkdir -p ~/.ssh'
  - 'echo -e "$DEPLOY_PRIVATE_KEY" > ~/.ssh/id_rsa'
  - 'chmod 600 ~/.ssh/id_rsa'
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" | tee  ~/.ssh/config'
  - 'apk add openssh' 
  script:
    - cp -r $CI_PROJECT_DIR/reports public/
    - 'ssh ubuntu@"${DEPLOY_SERVER_ID}" "ls -la && uname -a && mkdir -p /home/ubuntu/corporateapi"'
    - 'scp -r $CI_PROJECT_DIR/publishedcorpapi/* ubuntu@"${DEPLOY_SERVER_ID}":/home/ubuntu/corporateapi' 
  artifacts:
    paths:
      - public
    expire_in: 30 days
```



