﻿using CorporateAPI.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models.Response
{
    public class HourlyReportResponseModel
    {
        public HourlyReportDto ReportData { get; set; }

        public List<Message> Messages { get; set; }
    }
}
