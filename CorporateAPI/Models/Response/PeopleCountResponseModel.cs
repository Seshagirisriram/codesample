﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models.Response
{
    public class PeopleCountResponseModel
    {
        public int PezToday { get; set; }
        public int PezLy { get; set; }
        public decimal PezLyPercentageChange { get; set; }
        public int PezThisWeek { get; set; }
        public int PezThisWeekLy { get; set; }

        public decimal PezThisWeekLyPercentageChange { get; set; }

        public List<PezPreviousWeeks> PezPreviousWeeks;

        //public int Pez2Week { get; set; }
        //public int Pez2WeekLy { get; set; }
        //public decimal Pez2WeekLyPercentageChange { get; set; }
        //public int Pez4Week { get; set; }
        //public int Pez4WeekLy { get; set; }
        //public decimal Pez4WeekLyPercentageChange { get; set; }
        //public int Pez13Week { get; set; }
        //public int Pez13WeekLy { get; set; }
        //public decimal Pez13WeekLyPercentageChange { get; set; }
        public int PezYtd { get; set; }
        public int PezYtdLy { get; set; }
        public decimal PezYtdLyPercentageChange { get; set; }
        public int PezMtd { get; set; }
        public int PezMtdLy { get; set; }
        public decimal PezMtdLyPercentageChange { get; set; }
    }
    public class PezPreviousWeeks
    {
        public int PreviousWeek { get; set; }
        public int PezPreviousWeek { get; set; }
        public int PezPreviousWeekLy { get; set; }
        public decimal PezPreviousWeekLyPercentageChange { get; set; }
    }
}
