﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models.Response
{
    public class Message
    {
        public string Code { get; set; }
        public string Text { get; set; }

        public dynamic OtherDetails { get; set; }
    }
}
