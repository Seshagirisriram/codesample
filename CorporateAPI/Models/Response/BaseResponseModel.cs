﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models.Response
{
    public class BaseResponseModel
    {
        public int Id { get; set; }
        public String ResultCode { get; set; }
        public List<Message> Messages { get; set; }
    }
}
