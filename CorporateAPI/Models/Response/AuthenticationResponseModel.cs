﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models.Response
{
    public class AuthenticationResponseModel : BaseResponseModel
    {
        public String AuthenticationToken { get; set; }
    }
}
