﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CorporateAPI.Models
{
    public class ZoneListModel
    {
        
        public string Id { get; set; }
        [Required(ErrorMessage = "Camera name is required")]
        public string camera_name { get; set; }
        [Required(ErrorMessage = "Camera ip is required")]
        public string camera_ip { get; set; }
        public string camera_port { get; set; }
        public string camera_user { get; set; }
        public string camera_password { get; set; }
        public string camera_murl { get; set; }
        [Required(ErrorMessage = "Resolution is required")]
        public string annotation_resolution { get; set; }
        public List<ZoneDetailsListResponseModel> zones { get; set; }
    }
    public class ZoneDetailsListResponseModel
    {
        [Required(ErrorMessage = "Name is required")]
        public string name { get; set; }
        [Required(ErrorMessage = "Type is required")]
        public string type { get; set; }
        [Required(ErrorMessage = "Shape is required")]
        public string shape { get; set; }
        [Required(ErrorMessage = "Coordinates is required")]
        public string coordinates { get; set; }
        public string mapping_rule { get; set; }
        [Required(ErrorMessage = "Version is required")]
        public string version { get; set; }
        [Required(ErrorMessage = "Marking datetime is required")]
        [RegularExpression(@"^(\d{4}-\d{2}-\d{2}T\d{2}\:\d{2}\:\d{2}\.\d{3}Z)|(\d{4}-\d{2}-\d{2}T\d{2}\:\d{2}\:\d{2}Z)$", ErrorMessage = "Invalid date format")]
        public string marking_datetime { get; set; }
    }

}
