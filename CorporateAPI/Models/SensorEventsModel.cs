﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
using Newtonsoft.Json;

namespace CorporateAPI.Models
{
    [BsonIgnoreExtraElements]
    public class SensorEventsModel
    {
        [JsonIgnore]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string e { get; set; } // EventCode
        public string f { get; set; } // FrameId 
        public DateTime t { get; set; } // Timestamp
        public string o { get; set; } // ObjectId 
        public string c { get; set; } // Category
        public string z { get; set; }
        public string i { get; set; } // EvidenceImagePath
        public int n { get; set; }
        public string a { get; set; } // CameraId
    }

    public class EventDataModel
    {
        public string g { get; set; } // Gender

        public string a { get; set; } // AgeGroup

        public string e { get; set; } // Ethinicity

        public string r { get; set; } // Group
    }
  }
