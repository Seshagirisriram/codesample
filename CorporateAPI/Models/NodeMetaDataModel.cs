﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace CorporateAPI.Models
{
    [BsonIgnoreExtraElements]
    public class NodeMetaDataModel
    {
        [JsonIgnore]
        [BsonRepresentation(BsonType.ObjectId)]
       public string Id { get; set; }
       public int NodeId { get; set; }
       public string WeekBeginOn { get; set; }
       public int[] EntryZoneIDs { get; set; }
       public int[] ExitZoneIDs { get; set; }

    }
}
