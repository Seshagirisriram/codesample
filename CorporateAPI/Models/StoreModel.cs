﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models
{
    [BsonIgnoreExtraElements]
    public class StoreModel
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public int StoreId {get;set;}

        public int OrganizationId { get; set; }

        public int TagId { get; set; }

        public String Name { get; set; }

        public String StoreClientId { get; set; }

        public String TimeZone { get; set; }

        public TimeSpan StoreOpenTime { get; set; }

        public TimeSpan StoreCloseTime { get; set; }

        public int AllowedPeople { get; set; }

        public String Addresss { get; set; }

        public String GeoLocation { get; set; }

        public String BussinessContact { get; set; }

        public String TechnicalContact { get; set; }

        public String OnBoardedBy { get; set; }

        public DateTime? OnboardedOn { get; set; }

        public String AuthorizedBy { get; set; }

        public DateTime? AuthorizedOn { get; set; }

        public String Remarks { get; set; }

        public bool IsActive { get; set; }

        public DateTime? ActiveOn { get; set; }

        public DateTime? ExipiryDate { get; set; }

        public String ApplicableServices { get; set; }


    }
}
