﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models.Request
{
    public class PeopleCountRequestModel
    {
        public DateTime CurrentDate { get; set; }
        public int[] PreviousWeeks { get; set; }
        public int NodeId { get; set; }
    }
}
