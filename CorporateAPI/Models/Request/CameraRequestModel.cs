﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CorporateAPI.Models.Request
{
    public class CameraRequestModel
    {
        public int CameraId { get; set; }
        [Required(ErrorMessage = "Store Client Id is required")]
        public String StoreClientId { get; set; }

        public String CameraName { get; set; }
        [Required(ErrorMessage = "Camera IP is required")]
        [RegularExpression(@"^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$", ErrorMessage = "Invalid IPv4 address.")]
        public String CameraIp { get; set; }

        public String Protocol { get; set; }

        public String UserName { get; set; }

        public String Password { get; set; }

        public String Murl { get; set; }

        public String MurlSuffix { get; set; }
    }
}
