﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models.Request
{
    public class OrganizationRequestModel
    {
        public int OrganizationId { get; set; }

        public String Name { get; set; }

        public String Address { get; set; }

        public String BussinessContact { get; set; }

        public String TechnicalContact { get; set; } 

    }
}
