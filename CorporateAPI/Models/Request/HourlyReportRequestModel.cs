﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models.Request
{
    public class HourlyReportRequestModel
    {
        [Required(ErrorMessage ="Report Date cannot be empty")]
        public DateTime? ReportDate { get; set; }
        
        [Required(ErrorMessage ="Store Id cannot be empty")]
        public int? StoreId { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public int? Interval { get; set; }
    }

}
