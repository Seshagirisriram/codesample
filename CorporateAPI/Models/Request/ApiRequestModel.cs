﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models.Request
{
    public class ApiRequestModel
    {
        public int StoreId { get; set; }

        public String StoreClientId { get; set; }

        public List<ApiRequestEvent> ApiRequestEvents { get; set; }
    }
}
