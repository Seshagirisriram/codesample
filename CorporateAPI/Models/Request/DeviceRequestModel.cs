﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CorporateAPI.Models.Request
{
    public class DeviceRequestModel
    {
        public int DeviceId { get; set; }
        [Required(ErrorMessage = "Store Id is required")]
        public int StoreId { get; set; }
        [Required(ErrorMessage = "Type is required")]
        public String Type { get; set; }
        [Required(ErrorMessage = "Serial Number is required")]
        public String SerialNumber { get; set; }
       
        [RegularExpression(@"^(([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|((?:[0-9]{1,3}\.){3}[0-9]{1,3})$", ErrorMessage = "Invalid IP address.")]
        public String IpAddress { get; set; }

        public String MacAddress { get; set; }
    }
}
