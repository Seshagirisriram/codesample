﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CorporateAPI.Models.Request
{
    public class StoreRequestModel
    {

        public int StoreId { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public String Name { get; set; }
        [Required(ErrorMessage = "Store Client Id is required")]
        public String StoreClientId { get; set; }
        [Required(ErrorMessage = "Organization Id is required")]
        public int OrganizationId { get; set; }

        public int TagId { get; set; }
        [RegularExpression(@"^(?:Z|([+-])(\d{2}):?(\d{2}))$", ErrorMessage = "Invalid TimeZone.")]
        public String TimeZone { get; set; }
        [RegularExpression(@"^(2[0-3]|[01]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])$", ErrorMessage = "Invalid Time.")]

        public TimeSpan StoreOpenTime { get; set; }
        [RegularExpression(@"^(2[0-3]|[01]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])$", ErrorMessage = "Invalid Time.")]
        public TimeSpan StoreCloseTime { get; set; }
        [RegularExpression(@"^\d+$", ErrorMessage = "Invalid number.")]
        public int AllowedPeople { get; set; }

        public String Addresss { get; set; }

        public String GeoLocation { get; set; }

        public String BussinessContact { get; set; }

        public String TechnicalContact { get; set; }
    }
}
