﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CorporateAPI.Models.Request
{
    public class ZoneRequestModel
    {
        [Required(ErrorMessage = "Camera Id is required")]
        public int CameraId { get; set; }

        public int StoreId { get; set; }
        public int SystemZoneId { get; set; }

        public int PremiseZoneId { get; set; }

        public string SystemZoneName { get; set; }

        public string PremiseZoneName { get; set; }

        public string SceneCoordinates { get; set; }

        public string LayoutCoordinates { get; set; }

        public string ZoneClientId { get; set; }
        [RegularExpression(@"^(0|[1-9][0-9]{0,9})$", ErrorMessage = "Invalid number.")]
        public int InZoneCount { get; set; }
    }
}
