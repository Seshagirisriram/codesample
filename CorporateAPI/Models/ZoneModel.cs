﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models
{
    [BsonIgnoreExtraElements]
    public class ZoneModel
    {
        [JsonIgnore]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public int ZoneId { get; set; }
        public int CameraId { get; set; }

        public int StoreId { get; set; }

        public int SystemZoneId { get; set; }
        
        public int PremiseZoneId { get; set; }

        public string SystemZoneName { get; set; }

        public string PremiseZoneName { get; set; }

        public string SceneCoordinates { get; set; }

        public string LayoutCoordinates { get; set; }

        public string ZoneClientId { get; set; }

        public int InZoneCount { get; set; }

        public bool IsActive { get; set; }
    }
}
