﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models
{
    [BsonIgnoreExtraElements]
    public class OrganizationModel
    {
        [JsonIgnore]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public int OrganizationId { get; set; }

        public String Name { get; set; }

        public String Address { get; set; }

        public String BussinessContact { get; set; }

        public String TechnicalContact { get; set; }
        public bool IsActive { get; set; }
        public int NodeId { get; set; }
        public string NodeType { get; set; }
        public string OpenTime { get; set; }
        public string CloseTime { get; set; }
    }
}
