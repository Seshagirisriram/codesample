﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models
{
    [BsonIgnoreExtraElements]
    public partial class MongoAiviData
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string EventCode { get; set; }
        public long FrameID { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
        public Guid ObjectID { get; set; }
        public string Category { get; set; }
        public SpatialData SpatialData { get; set; }
        public EventData EventData { get; set; }
        public string Zone { get; set; }
        public long StoreId { get; set; }
        public long CameraId { get; set; }

        public int CurrentInStoreCount { get; set; }


    }

    public partial class EventData
    {
        public object Gender { get; set; }
        public object AgeString { get; set; }
        public object Ethnicity { get; set; }
        public object Zone { get; set; }
    }

    public partial class SpatialData
    {
        public string Confidence { get; set; }
        public long X { get; set; }
        public long Y { get; set; }
        public long H { get; set; }
        public long W { get; set; }
    }
}
