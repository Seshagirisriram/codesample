﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models
{
    [BsonIgnoreExtraElements]
    public class CameraModel
    {
        [JsonIgnore]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public int StoreId { get; set; }
        public string StoreClientId { get; set; }
        public int CameraId { get; set; }

        public String CameraName { get; set; }
        public String CameraIp { get; set; }

        public String Protocol { get; set; }

        public String UserName { get; set; }

        public String Password { get; set; }

        public String Murl { get; set; }

        public String MurlSuffix { get; set; }

        public bool IsActive { get; set; }

        public string CameraSourceFilePath { get; set; }
    }
}
