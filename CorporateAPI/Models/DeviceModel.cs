﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models
{
    [BsonIgnoreExtraElements]
    public class DeviceModel
    {
        [JsonIgnore]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public int DeviceId { get; set; }

        public int StoreId { get; set; }

        public String Type { get; set; }

        public String SerialNumber { get; set; }

        public String IpAddress { get; set; }

        public String MacAddress { get; set; }

        public bool IsActive { get; set; }
    }
}
