﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Models
{

    [BsonIgnoreExtraElements]
    public class StoreApiRequestMaster
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public int StoreId { get; set; }

        public String StoreClientId { get; set; }

        public List<ApiRequestEvent> ApiRequests {get;set;}    
    }


    public class ApiRequestEvent { 
        
        public String Event { get; set; }

        public List<ApiRequest> ApiRequests { get; set; }

    }

    public class ApiRequest { 
        public String Type { get; set; }


        public bool IsAuthenticateRequest { get; set; }

        public string Url { get; set; }

        public string RequestType { get; set; }

        public String UserName { get; set; }

        public String Password { get; set; }

    }
}
