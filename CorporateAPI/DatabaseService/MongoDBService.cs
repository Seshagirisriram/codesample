﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CorporateAPI.DatabaseService
{
    /// <summary>
    /// Mongo Db Helper Class
    /// </summary>
    /// <typeparam name="T">Generic Type for Name of Service</typeparam>
    public class MongoDBService<T> : IMongoDBService<T>
        where T : class
    {
        /// <summary>
        /// Mongo DB Client Instance
        /// </summary>
        private MongoClient client;

        /// <summary>
        /// MongoDB Database Instance
        /// </summary>
        private IMongoDatabase database;

        /// <summary>
        /// MongoDb Collection Instance of type T <see cref="T"/>
        /// </summary>
        private IMongoCollection<T> collection;

        // private bool isConnectionOpened; // This is never used... 

        /// <summary>
        /// Establishing connection to the database
        /// </summary>
        public void Open()
        {
            if (this.client == null)
            {
                this.client = new MongoDBHelper<MongoDBService<T>>().getClientFromConfig(); 
            }
            var clientDB = "cicdswarak"; //System.Configuration.ConfigurationManager.AppSettings["MongoClientDatabaseName"];
            this.database = this.client.GetDatabase(clientDB);
            this.collection = this.database.GetCollection<T>("aividata");
        }

        /// <summary>
        /// Establishing connection to the database and opening the collection
        /// </summary>
        /// <param name="collectionName">Connection String</param>
        public void Open(string collectionName)
        {
            if (this.client == null)
            {
                this.client = new MongoDBHelper<MongoDBService<T>>().getClientFromConfig();
            }
            var clientDB = "cicdswarak"; //System.Configuration.ConfigurationManager.AppSettings["MongoClientDatabaseName"];
            this.database = this.client.GetDatabase(clientDB);
            this.collection = this.database.GetCollection<T>(collectionName);
        }

        /// <summary>
        /// Execute the Query
        /// </summary>
        /// <param name="filterDefinition">The filter definition to perform a find/Query</param>
        /// <returns>Enumerable Collection based on filterDefinition</returns>
        public IEnumerable<T> Find(FilterDefinition<T> filterDefinition)
        {
            this.Open();
            return this.collection.Find(filterDefinition).ToList();
        }

        /// <summary>
        /// Execute the Query
        /// </summary>
        /// <param name="collectionName">The name of the Collection to perform a query on</param>
        /// <param name="Expression">The Expression to perform a search</param>
        /// <returns>Collection on CollectionName and Expression</returns>
        public IEnumerable<T> Find(string collectionName, Expression<Func<T, bool>> where)
        {
            this.Open(collectionName);
            return this.collection.Find(where).ToList();
        }

        /// <summary>
        /// Find and Increment
        /// </summary>
        /// <param name="sequenceValue">Given a sequence Value(?? name perhaps), find the value and increment it</param>
        /// <returns>Incremented Sequence value</returns>
        public int FindAndIncrement(string sequenceValue)
        {
            if (this.client == null)
            {
                this.client = new MongoDBHelper<MongoDBService<T>>().getClientFromConfig();
            }
            var clientDB = "cicdswarak"; //System.Configuration.ConfigurationManager.AppSettings["MongoClientDatabaseName"];
            this.database = this.client.GetDatabase(clientDB);
            var collection = this.database.GetCollection<BsonDocument>("Counter");
            var filter = Builders<BsonDocument>.Filter.Eq(x => x["_id"], sequenceValue);
            var update = Builders<BsonDocument>.Update.Inc("sequence_value", 1);
            BsonValue value =  collection.FindOneAndUpdateAsync(filter, update).Result.GetValue("sequence_value");
            return value.ToInt32();
        }

        /// <summary>
        /// Custom Search Conditions
        /// </summary>
        /// <param name="startDate">Start of Date Range to perform search </param>
        /// <param name="endDate">End of Date Range to perform search </param>
        /// <returns>Collection of search results. </returns>
        public IEnumerable<T> FetchAiviDataDateSearch(DateTime startDate, DateTime endDate)
        {
            this.Open();
            var projectDefintion = Builders<BsonDocument>.Projection.Exclude("timeStampAsDate");
            var expression = new BsonDocument(new List<BsonElement>
            {
                new BsonElement("timeStampAsDate", new BsonDocument(new BsonElement("$toDate", "$TimeStamp"))),
            });
            var addFieldStage = new BsonDocument(new BsonElement("$addFields", expression));
            var gteFilter =
                Builders<BsonDocument>.Filter.Gte(x => x["timeStampAsDate"], new BsonDateTime(startDate));
            var lteFilter =
                Builders<BsonDocument>.Filter.Lte(x => x["timeStampAsDate"], new BsonDateTime(endDate));
            var result = this.collection.Aggregate().AppendStage<BsonDocument>(addFieldStage)
                .Match(gteFilter).Match(lteFilter).Project<BsonDocument>(projectDefintion).As<T>();
            var docs = result;
            return result.ToList();
        }

        /// <summary>
        /// Insert the Records into the MongoDB
        /// </summary>
        /// <param name="SyncData">Data of Type <see cref="T"/></param>
        /// <param name="collectionName">Name of collection to search on</param>
        /// <returns>Asynchronous boolean value</returns>
        public async Task<bool> InsertRecords(T SyncData, string collectionName)
        {
            try
            {
                this.Open(collectionName);
                await this.collection.InsertOneAsync(SyncData).ConfigureAwait(true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return true;
        }

        /// <summary>
        /// Insert the Records into the MongoDB
        /// </summary>
        /// <param name="SyncData">Data of Type <see cref="T"/> </param>
        /// <param name="CollectionName">name of collection to search on</param>
        /// <param name="where">true or false</param>
        /// <returns>Asynchronous boolean value</returns>
        public async Task<bool> UpdateRecords(T SyncData, string CollectionName, Expression<Func<T, bool>> where)
        {
            try
            {
                this.Open(CollectionName);
                await this.collection.ReplaceOneAsync<T>(where, SyncData).ConfigureAwait(true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return true;
        }
    }
}
