﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace CorporateAPI.DatabaseService
{
    public interface IMongoDBService<T> where T : class
    {
        void Open();
        void Open(String CollectionName);
        IEnumerable<T> Find(FilterDefinition<T> filterDefinition);
        IEnumerable<T> Find(String CollectionName, Expression<Func<T, bool>> where);
        int FindAndIncrement(String sequenceValue);
        IEnumerable<T> FetchAiviDataDateSearch(DateTime startDate, DateTime endDate);
        Task<bool> InsertRecords(T SyncData, String CollectionName);
        Task<bool> UpdateRecords(T SyncData, String CollectionName, Expression<Func<T, bool>> where);
    }
}

