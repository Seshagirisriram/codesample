#Readme file for Corporate APIs Postman Collection and Runner

## To run the file from commandline:
1. Ensure that the newman npm package is installed. If not, install by running npm install -g newman
2. run the newman_run.bat file.

## How to update the collection
1. From postman, export the latest collection. This will download the collection json file.
2. From postman, export the desired environment. This will download the environment json file.