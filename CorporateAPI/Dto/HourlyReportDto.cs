﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Dto
{
    public class HourlyReportDto
    {
        public int StoreId { get; set; }

        public string Date { get; set; }

        public List<FootFallHourlyDetailsDto> FootFall { get; set; }

    }

}
