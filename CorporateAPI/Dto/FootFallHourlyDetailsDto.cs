﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Dto
{
    public class FootFallHourlyDetailsDto 
    {
        public String StartTime { get; set; }
        public String EndTime { get; set; }
        public int TotalIn { get; set; }
        public int TotalOut { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public int Average { get; set; }
        public int Allowed { get; set; }        
    }
}
