﻿using CorporateAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Dto
{
    public class StoreDetailsDto : StoreModel
    {
        public List<CameraDetailsDto> Cameras { get; set; }
    }

    public class CameraDetailsDto : CameraModel {

        public List<ZoneModel> Zones { get; set; }
    }
}
