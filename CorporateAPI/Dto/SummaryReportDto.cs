﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Dto
{
    public class SummaryReportDto
    {
        public int StoreId { get; set; }

        public List<FootFallDetailsDto> FootFall { get; set; }
    }
}
