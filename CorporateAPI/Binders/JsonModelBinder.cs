﻿namespace CorporateAPI.ModelBinders
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.ModelBinding;

    /// <summary>
    /// Deals with the Extraction of the Json Content from the request
    /// </summary>
    public class JsonModelBinder : IModelBinder
    {
        /// <summary>
        /// Bind the JsonModel to Model Binding Context and extract the json data
        /// </summary>
        /// <param name="bindingContext">What does bindingContext mean?explain.</param>
        /// <returns>Task</returns>
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            // Check the value sent in
            var valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (valueProviderResult != ValueProviderResult.None)
            {
                bindingContext.ModelState.SetModelValue(bindingContext.ModelName, valueProviderResult);

                // Attempt to convert the input value
                var valueAsString = valueProviderResult.FirstValue;
                var result = Newtonsoft.Json.JsonConvert.DeserializeObject(valueAsString, bindingContext.ModelType);
                if (result != null)
                {
                    bindingContext.Result = ModelBindingResult.Success(result);
                    return Task.CompletedTask;
                }

                // SRIRAM: what happens when valueProviderResult === ValueProviderResult.None??
            }

            return Task.CompletedTask;
        }
    }
}
