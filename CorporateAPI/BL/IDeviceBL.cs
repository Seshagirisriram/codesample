﻿using System;
using CorporateAPI.Dto;
using CorporateAPI.Exceptions;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.BL
{
    public interface IDeviceBL
    {
        Task<bool> AddDevice(DeviceModel deviceModel);
        Task<bool> UpdateDevice(DeviceModel deviceModel);
        Task<DeviceModel> GetDevice(int DeviceId);
        DeviceModel GetDeviceBySerialNumber(String serialNumber);
        Task<DeviceModel> AddDevice(DeviceRequestModel deviceRequestModel);
        DeviceModel GetTypeBySerialNumber(String serialNumber, String type);
        List<DeviceModel> GetDevices(int storeId);
    }
}
