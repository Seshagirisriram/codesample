﻿using System;
using CorporateAPI.Dto;
using CorporateAPI.Exceptions;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using CorporateAPI.Models.Response;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.BL
{
    public interface IPeopleCountBL
    {
        PeopleCountResponseModel GetPeopleCountDetails(PeopleCountRequestModel requestModel);
    }
}
