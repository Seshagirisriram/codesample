﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorporateAPI.Models.Request;
using CorporateAPI.Models.Response;
using CorporateAPI.Models;
using System.Configuration;
using CorporateAPI.Helpers;
using log4net;
using CorporateAPI.DatabaseService;

namespace CorporateAPI.BL
{
    public class PeopleCountBL:IPeopleCountBL
    {
        /// <summary>
        /// Get the People Count details in a store/organization.
        /// </summary>
        private IMongoDBService<PeopleCountRequestModel> peopleRepository { get; set; }

        public PeopleCountBL(IMongoDBService<PeopleCountRequestModel> peopleRepository)
        {
            peopleRepository ??= new MongoDBService<PeopleCountRequestModel>();
        }

        public PeopleCountResponseModel GetPeopleCountDetails(PeopleCountRequestModel requestModel)
        {
            PeopleCountResponseModel peopleCountModel = null;
            MongoDBHelper<NodeMetaDataModel> mongoDBHelper = new MongoDBHelper<NodeMetaDataModel>();
            List<NodeMetaDataModel> nodeModels = mongoDBHelper.Find("nodemetadata", x => x.NodeId==requestModel.NodeId);
            MongoDBHelper<SensorEventsModel> mongoDBHelperSensor = new MongoDBHelper<SensorEventsModel>();
            List<SensorEventsModel> sensorEventsModels = mongoDBHelperSensor.Find("sensorevents", x => x.n == requestModel.NodeId);
            MongoDBHelper<OrganizationModel> mongoDBHelperOrganization = new MongoDBHelper<OrganizationModel>();
            var nodeType = ConfigurationManager.AppSettings["LeafNodeType"].ToString();
            List<OrganizationModel> organizationModels = mongoDBHelperOrganization.Find("organizationmaster", x => x.NodeId == requestModel.NodeId && x.NodeType==nodeType);

            DateTime currentDateTime = requestModel.CurrentDate;
            DateTime thisHour = Convert.ToDateTime(currentDateTime.ToString("hh:mm:ss"));
            DateTime OpenTime = Convert.ToDateTime(organizationModels[0].OpenTime);
            DateTime CloseTime = Convert.ToDateTime(organizationModels[0].CloseTime);

            // Count of Person Enter Zone Today
            int countEnterZoneToday = sensorEventsModels.Count(a => a.e == "PEZ" &&
                          (OpenTime > thisHour && CloseTime < thisHour) &&
                          (a.z == nodeModels[0].EntryZoneIDs[0].ToString() ||
                          a.z == nodeModels[0].EntryZoneIDs[1].ToString()));

            // Count of Person Left Zone Today
            int countLeftZoneToday = sensorEventsModels.Count(a => a.e == "PLZ" &&
                         (OpenTime > thisHour ) &&
                         (a.z == nodeModels[0].EntryZoneIDs[0].ToString() ||
                         a.z == nodeModels[0].EntryZoneIDs[1].ToString()));
            int peopleCountToday = countEnterZoneToday - countLeftZoneToday;

            // Count of Person Enter Zone Last Year
            int countEnterZoneTodayLy = sensorEventsModels.Count(a => a.e == "PEZ" &&
                     (a.t == currentDateTime.AddYears(-1)) &&
                     (a.z == nodeModels[0].EntryZoneIDs[0].ToString() ||
                     a.z == nodeModels[0].EntryZoneIDs[1].ToString()));

            decimal countTodayLYPercentageChange = 0;
            if (countEnterZoneTodayLy > 0)
            {
                countTodayLYPercentageChange = (peopleCountToday - countEnterZoneTodayLy) / countEnterZoneTodayLy;
            }

            // Person Enter Zone This Week
            DayOfWeek weekDay = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), nodeModels[0].WeekBeginOn);
            DateTime thisWeekStartDate = currentDateTime.StartOfWeek(weekDay);

            // Count of Person Enter Zone This Week
            var countEnterZoneThisWeek = sensorEventsModels.Count(a => a.e == "PEZ" &&
                         (a.t >= thisWeekStartDate && a.t <= currentDateTime) &&
                         (a.z == nodeModels[0].EntryZoneIDs[0].ToString() ||
                         a.z == nodeModels[0].EntryZoneIDs[1].ToString()));

            // Count of Person Enter Zone  This Week Last Year
            DateTime thisWeekStartDateLy = currentDateTime.AddYears(-1).StartOfWeek(weekDay);
            var countEnterZoneThisWeekLy = sensorEventsModels.Count(a => a.e == "PEZ" &&
                         (a.t >= thisWeekStartDateLy && a.t <= currentDateTime.AddYears(-1)) &&
                         (a.z == nodeModels[0].EntryZoneIDs[0].ToString() ||
                         a.z == nodeModels[0].EntryZoneIDs[1].ToString()));

            decimal countThisWeekLyPercentageChange = 0;
            if (countEnterZoneThisWeekLy > 0)
            {
                countThisWeekLyPercentageChange = (countEnterZoneThisWeek - countEnterZoneThisWeekLy) / countEnterZoneThisWeekLy;
            }

            List<PezPreviousWeeks> countEntryZonePreviousWeeks = new List<PezPreviousWeeks>();
            PezPreviousWeeks countPreviousWeek = null;
            foreach (var week in requestModel.PreviousWeeks)
            {
                countPreviousWeek = new PezPreviousWeeks();
                countPreviousWeek.PreviousWeek = week;

                // Person enter zone previous weeks
                countPreviousWeek.PezPreviousWeek = sensorEventsModels.Count(a => a.e == "PEZ" &&
                             (a.t >= currentDateTime.AddDays(-7 * (week))) &&
                             (a.z == nodeModels[0].EntryZoneIDs[0].ToString() ||
                             a.z == nodeModels[0].EntryZoneIDs[1].ToString()));

                // Person enter zone previous weeks Last Year
                countPreviousWeek.PezPreviousWeekLy = sensorEventsModels.Count(a => a.e == "PEZ" &&
                             (a.t >= currentDateTime.AddYears(-1).AddDays(-7 * (week)) && a.t <= currentDateTime.AddYears(-1)) &&
                             (a.z == nodeModels[0].EntryZoneIDs[0].ToString() ||
                             a.z == nodeModels[0].EntryZoneIDs[1].ToString()));
                if (countPreviousWeek.PezPreviousWeekLy > 0)
                {
                    countPreviousWeek.PezPreviousWeekLyPercentageChange = ((countPreviousWeek.PezPreviousWeek - countPreviousWeek.PezPreviousWeekLy) / countPreviousWeek.PezPreviousWeekLy);
                }

                countEntryZonePreviousWeeks.Add(countPreviousWeek);
            }

            // Person enter zone year till date
            int year = currentDateTime.Year;
            DateTime firstDay = new DateTime(year, 1, 1);
            var countEnterZoneYtd = sensorEventsModels.Count(a => a.e == "PEZ" &&
                         (a.t >= firstDay && a.t <= currentDateTime) &&
                         (a.z == nodeModels[0].EntryZoneIDs[0].ToString() ||
                         a.z == nodeModels[0].EntryZoneIDs[1].ToString()));

            // Count of Person enter zone year till date Last Year
            var countEnterZoneYtdLy = sensorEventsModels.Count(a => a.e == "PEZ" &&
                         (a.t >= firstDay.AddYears(-1) && a.t <= currentDateTime.AddYears(-1)) &&
                         (a.z == nodeModels[0].EntryZoneIDs[0].ToString() ||
                         a.z == nodeModels[0].EntryZoneIDs[1].ToString()));

            decimal countYtdLyPercentageChange = 0;
            if (countEnterZoneYtdLy > 0)
            {
                countYtdLyPercentageChange = ((countEnterZoneYtd - countEnterZoneYtdLy) / countEnterZoneYtdLy);
            }

            // Person enter zone month till date.
            var StartMonthDate = new DateTime(currentDateTime.Year, currentDateTime.Month, 1);

            // Count of Person enter zone month till date.
            var countEnterZoneMtd = sensorEventsModels.Count(a => a.e == "PEZ" &&
                          (a.t >= StartMonthDate && a.t <= currentDateTime) &&
                         (a.z == nodeModels[0].EntryZoneIDs[0].ToString() ||
                         a.z == nodeModels[0].EntryZoneIDs[1].ToString()));

            // Count of Person enter zone month till date Last Year
            var countEnterZoneMtdLy = sensorEventsModels.Count(a => a.e == "PEZ" &&
                         (a.t >= StartMonthDate.AddYears(-1) && a.t <= currentDateTime.AddYears(-1)) &&
                         (a.z == nodeModels[0].EntryZoneIDs[0].ToString() ||
                         a.z == nodeModels[0].EntryZoneIDs[1].ToString()));

            decimal countMtdLyPercentageChange = 0;
            if (countEnterZoneMtdLy > 0)
            {
                countMtdLyPercentageChange = ((countEnterZoneMtd - countEnterZoneMtdLy) / countEnterZoneMtdLy);
            }

            peopleCountModel = new PeopleCountResponseModel();
            peopleCountModel.PezToday = countEnterZoneToday;
            peopleCountModel.PezLy = countEnterZoneTodayLy;
            peopleCountModel.PezLyPercentageChange = countTodayLYPercentageChange;
            peopleCountModel.PezThisWeek = countEnterZoneThisWeek;
            peopleCountModel.PezThisWeekLy = countEnterZoneThisWeekLy;
            peopleCountModel.PezThisWeekLyPercentageChange = countThisWeekLyPercentageChange;
            peopleCountModel.PezPreviousWeeks = countEntryZonePreviousWeeks;
            peopleCountModel.PezYtd = countEnterZoneYtd;
            peopleCountModel.PezYtdLy = countEnterZoneYtdLy;
            peopleCountModel.PezYtdLyPercentageChange = countYtdLyPercentageChange;
            peopleCountModel.PezMtd = countEnterZoneMtd;
            peopleCountModel.PezMtdLy = countEnterZoneMtdLy;
            peopleCountModel.PezMtdLyPercentageChange = countMtdLyPercentageChange;

            return peopleCountModel;
        }

    }
}
