﻿namespace CorporateAPI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CorporateAPI.DatabaseService;
    using CorporateAPI.Models;
    using CorporateAPI.Models.Request;
    using log4net;

    /// <summary>
    /// Document this.. 
    /// </summary>
    public class CameraBL : ICameraBL
    {
        /// <summary>
        /// Static Logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(CameraBL));
        
        /// <summary>
        /// Camerarepository
        /// </summary>
        private IMongoDBService<CameraModel> CameraRepository { get; set; }

        /// <summary>
        /// Storerepository
        /// </summary>
        private IMongoDBService<StoreModel> StoreRepository { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CameraBL"/> class.
        /// </summary>
        public CameraBL()
        {
            this.CameraRepository ??= new MongoDBService<CameraModel>();
            this.StoreRepository ??= new MongoDBService<StoreModel>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CameraBL"/> class.
        /// <paramref name="cameraRepository">What is this?</paramref>
        /// <paramref name="storeRepository">What is this?</paramref>
        /// </summary>
        public CameraBL(IMongoDBService<CameraModel> cameraRepository, IMongoDBService<StoreModel> storeRepository)
        {
            if (cameraRepository == null)
            {
                this.CameraRepository ??= new MongoDBService<CameraModel>();
            }
            else
            {
                this.CameraRepository = cameraRepository; 
            }
            if (storeRepository == null)
            {
                this.StoreRepository ??= new MongoDBService<StoreModel>();
            }
            else
            {
                this.StoreRepository = storeRepository; 
            }
        }

        public async Task<CameraModel> AddCamera(CameraRequestModel cameraRequestModel) {
            StoreModel store = new StoreBL(StoreRepository).GetStore(cameraRequestModel.StoreClientId);
            CameraModel cameraModel = new CameraModel()
            {
                CameraIp = cameraRequestModel.CameraIp,
                Password = cameraRequestModel.Password,
                Protocol = cameraRequestModel.Protocol,
                UserName = cameraRequestModel.UserName,
                StoreId=store.StoreId,
                CameraName=cameraRequestModel.CameraName,
                Murl=cameraRequestModel.Murl,
                MurlSuffix=cameraRequestModel.MurlSuffix,
                StoreClientId=cameraRequestModel.StoreClientId,
 
            };
            cameraModel.CameraId = this.CameraRepository.FindAndIncrement("camera_id");
            bool isInserted = await this.CameraRepository.InsertRecords(cameraModel, "cameramaster").ConfigureAwait(true);
            CameraModel cameraModelResult = null;
            if (isInserted) { cameraModelResult = cameraModel; }
            return cameraModelResult;
        }

        /// <summary>
        /// Get the camera list based on the Id of the store
        /// </summary>
        /// <param name="StoreId">Id of the Store</param>
        /// <returns>List of camera details</returns>
        public List<CameraModel> GetCameras(int storeId) {
            try
            {
                List<CameraModel> cameras = this.CameraRepository.Find("cameramaster", x => x.StoreId == storeId && x.IsActive == true).ToList();
                return cameras;
            } 
            catch (Exception e) 
            // SRIRAM: Avoid generic Exceptions..
            {
                Log.Error(e.Message);
                return new List<CameraModel>(); // SRIRAM: Is this a valid strategy??? 
            }
        }


        /// <summary>
        /// Get the Camera based on the camera Id
        /// </summary>
        /// <param name="cameraId">Id of the camera</param>
        /// <returns>List of camera details</returns>
        public CameraModel GetCamera(int cameraId)
        {
            CameraModel cameraModel = null;
            List<CameraModel> cameras = CameraRepository.Find("cameramaster", x => x.CameraId == cameraId && x.IsActive == true).ToList();
            if (cameras.Count > 0) { cameraModel = cameras[0]; }
            return cameraModel;
        }

        /// <summary>
        /// Get the IP Address By Store Client Id
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        public CameraModel GetIpAddressByStoreId(string cameraIp, int storeId)
        {
            CameraModel cameraModel = null;
            if (!String.IsNullOrEmpty(cameraIp))
            {
                List<CameraModel> cameras = CameraRepository.Find("cameramaster", x => (x.CameraIp.ToLower() == cameraIp.ToLower() && x.StoreId == storeId) && (x.IsActive == true)).ToList();
                if (cameras.Count > 0) { cameraModel = cameras[0]; }
            }
            return cameraModel;
        }

        /// <summary>
        /// Get the Camera details By Store Client Id
        /// </summary>
        /// <param name="cameraRequestModel"></param>
        /// <returns></returns>
        public CameraModel GetCameraByStoreClientId(CameraRequestModel cameraRequestModel)
        {
            CameraModel cameraModel = null;
            if (!String.IsNullOrEmpty(cameraRequestModel.StoreClientId))
            {
                StoreModel store = new StoreBL(StoreRepository).GetStore(cameraRequestModel.StoreClientId);
                List<CameraModel> cameras = CameraRepository.Find("cameramaster", x => x.StoreId == store.StoreId && x.IsActive == true).ToList();
                if (cameras.Count > 0) { cameraModel = cameras[0]; }
            }
            return cameraModel;
        }
        /// <summary>
        /// Update the camera by Camera Id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> UpdateCamera(CameraModel model)
        {
            bool IsUpdated = await CameraRepository.UpdateRecords(model, "cameramaster", x => x.CameraId == model.CameraId);
            return IsUpdated;
        }
        /// <summary>
        /// Get the IP Address By Camera Id
        /// </summary>
        /// <param name="cameraId"></param>
        /// <returns></returns>
        public CameraModel GetIpAddressByCameraId(string cameraIp, int cameraId)
        {
            CameraModel cameraModel = null;
           
                List<CameraModel> cameras = CameraRepository.Find("cameramaster", x => (x.CameraIp == cameraIp && x.CameraId == cameraId) && (x.IsActive == true)).ToList();
                if (cameras.Count > 0) { cameraModel = cameras[0]; }
           
            return cameraModel;
        }



    }
}
