﻿using CorporateAPI.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using CorporateAPI.Dto;
using CorporateAPI.Models;

namespace CorporateAPI.BL
{
    public class HourlyReportBL
    {
        /// <summary>
        /// Static Logger
        /// </summary>
        private static ILog Log = LogManager.GetLogger(typeof(HourlyReportBL));

        public HourlyReportDto GetReports(HourlyReportRequestModel requestModel) {
            HourlyReportDto HourlyReportData = new HourlyReportDto();
            try
            {
                MongoDBHelper<MongoAiviData> mongoDBHelper = new MongoDBHelper<MongoAiviData>();

                TimeSpan StartTimeSpan = Globals.StoreReportDefaultStartTime;
                TimeSpan EndTimeSpan = Globals.StoreReportDefaultEndTime;

                if (requestModel.Interval == null) { requestModel.Interval = Globals.StoreReportDefaultInterval; }

                if (requestModel.StartTime != null) { StartTimeSpan = (TimeSpan)requestModel.StartTime; }
                if (requestModel.EndTime != null) { EndTimeSpan = (TimeSpan)requestModel.EndTime; }


                DateTime startDateTime = (DateTime)requestModel.ReportDate;
                DateTime endDateTime = startDateTime.Date.AddDays(1);
                //TimeZoneInfo.Local.BaseUtcOffset.Hours, TimeZoneInfo.Local.BaseUtcOffset.Minutes
                var StartDateWithOffset = new DateTime(startDateTime.Year, startDateTime.Month, startDateTime.Day,
                    StartTimeSpan.Hours,StartTimeSpan.Minutes,StartTimeSpan.Seconds,
                  0);
                var EndDateWithOffset = new DateTime(endDateTime.Year, endDateTime.Month, endDateTime.Day,
                  EndTimeSpan.Hours, EndTimeSpan.Minutes, EndTimeSpan.Seconds);
                List<MongoAiviData> mongoAiviDatas = mongoDBHelper.FetchAiviDataDateSearch(StartDateWithOffset, EndDateWithOffset);
                List<MongoAiviData> FilteredStoreWise = mongoAiviDatas.Where(x => x.StoreId == requestModel.StoreId ).ToList();

                DateTimeOffset FilteredMinDateTime = FilteredStoreWise.Min(x => x.TimeStamp);
                DateTimeOffset FilteredMaxDateTime = FilteredStoreWise.Max(x => x.TimeStamp);
                int TotalSeconds = (int)((FilteredMaxDateTime.TimeOfDay - StartTimeSpan)).TotalSeconds;

                HourlyReportData.StoreId = (int)requestModel.StoreId;
                HourlyReportData.Date = ((DateTime)requestModel.ReportDate).ToString("yyyy-MM-dd");

                HourlyReportData.FootFall = new List<FootFallHourlyDetailsDto>();


                for (int Seconds = 0; Seconds < TotalSeconds;) {
                    int TempSecond = Seconds;
                    Seconds = Seconds + (int)requestModel.Interval;
                    DateTimeOffset TempStartTime = StartDateWithOffset.AddSeconds(TempSecond);
                    DateTimeOffset TempEndTime = StartDateWithOffset.AddSeconds(Seconds);
                    List<MongoAiviData> SecondWiseData = FilteredStoreWise.Where(x => x.TimeStamp > TempStartTime && x.TimeStamp <= TempEndTime).ToList();
                    FootFallHourlyDetailsDto Dto = new FootFallHourlyDetailsDto();
                    if (SecondWiseData.Count > 0)
                    {
                        Dto.Allowed = 15;
                        Dto.Average = (int)SecondWiseData?.Average(x => x.CurrentInStoreCount);
                        Dto.TotalIn = SecondWiseData.Where(x => x.EventCode == "PEZ").Count();
                        Dto.TotalOut = SecondWiseData.Where(x => x.EventCode == "PLZ").Count();
                        Dto.Min = 1;
                        Dto.Max = SecondWiseData.Max(x => x.CurrentInStoreCount);
                        Dto.StartTime = TempStartTime.ToString("HH:mm:ss");
                        Dto.EndTime = TempEndTime.ToString("HH:mm:ss");
                    }
                    else {
                        Dto.Allowed = 15;
                        Dto.Average = 0;
                        Dto.TotalIn = 0;
                        Dto.TotalOut = 0;
                        Dto.Min = 0;
                        Dto.Max = 0;
                        Dto.StartTime = TempStartTime.ToString("HH:mm:ss");
                        Dto.EndTime = TempEndTime.ToString("HH:mm:ss");
                    }
                    HourlyReportData.FootFall.Add(Dto);
                }
                
                //foreach (var group in DateWiseGrouping) {
                //    dateWise.Hourly.Add(group);                
                //}

            }
            catch (Exception e) {
                Log.Error(e.StackTrace);
            }
            return HourlyReportData;
        }
    }
}
