﻿using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorporateAPI.DatabaseService;

namespace CorporateAPI.BL
{
    public class ZonesBL:IZonesBL
    {
        private IMongoDBService<ZoneModel> zoneRepository { get; set; }
        public ZonesBL(IMongoDBService<ZoneModel> ZoneRepository)
        {
            zoneRepository ??= new MongoDBService<ZoneModel>();
        }
        /// <summary>
        /// Add Zone to the camera
        /// </summary>
        /// <param name="zoneRequestModel">Zone Request Model</param>
        /// <returns>Zone details</returns>
        public async Task<ZoneModel> AddZone(ZoneRequestModel zoneRequestModel) {
         
            ZoneModel zoneModel = new ZoneModel()
            {
                CameraId = zoneRequestModel.CameraId,
                LayoutCoordinates = zoneRequestModel.LayoutCoordinates,
                PremiseZoneId = zoneRequestModel.PremiseZoneId,
                PremiseZoneName = zoneRequestModel.PremiseZoneName,
                SceneCoordinates = zoneRequestModel.SceneCoordinates,
                SystemZoneId = zoneRequestModel.SystemZoneId,
                SystemZoneName = zoneRequestModel.SystemZoneName,
                InZoneCount = zoneRequestModel.InZoneCount,
                ZoneClientId = zoneRequestModel.ZoneClientId,
                StoreId=zoneRequestModel.StoreId

            };
            zoneModel.ZoneId = zoneRepository.FindAndIncrement("zone_id");
            bool IsSaved= await zoneRepository.InsertRecords(zoneModel,"zonemaster");
            ZoneModel zoneResult = null;
            if (IsSaved) { zoneResult = zoneModel; }
            return zoneResult;
        }

        /// <summary>
        /// Function used to get the Zones based on the cameraId
        /// </summary>
        /// <param name="cameraId">Id of the camera</param>
        /// <returns>List of Zone Models</returns>
        public List<ZoneModel> GetZones(int cameraId) {
            List<ZoneModel> zones = zoneRepository.Find("zonemaster", x => x.CameraId == cameraId && x.IsActive == true).ToList();
            return zones;
        }


        /// <summary>
        /// Function used to get the Zones based on the zoneId
        /// </summary>
        /// <param name="zoneId">Id of the Zone</param>
        /// <returns>List of Zone Models</returns>
        public ZoneModel GetZone(int zoneId)
        {
            ZoneModel zoneModel = null;
            List<ZoneModel> zones = zoneRepository.Find("zonemaster", x => x.ZoneId == zoneId ).ToList();
            if (zones.Count > 0) { zoneModel = zones[0]; }
            return zoneModel;
        }
        /// <summary>
        /// Function used to get the Zone name based on the cameraId
        /// </summary>
        /// <param name="cameraId">cameraId</param>
        /// <returns>List of Zone Models</returns>
        public ZoneModel GetZone(string zone,int cameraId)
        {
            ZoneModel zoneModel = null;
            List<ZoneModel> zones = zoneRepository.Find("zonemaster", x => (x.SystemZoneName==zone && x.CameraId == cameraId) && (x.IsActive==true)).ToList();
            if (zones.Count > 0) { zoneModel = zones[0]; }
            return zoneModel;
        }
       
        /// <summary>
        /// Update the zone by Zone Id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> UpdateZone(ZoneModel model)
        {
            bool IsUpdated = await zoneRepository.UpdateRecords(model, "zonemaster", x => x.ZoneId == model.ZoneId);
            return IsUpdated;
        }

    }
}
