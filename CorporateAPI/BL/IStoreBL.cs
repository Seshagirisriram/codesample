﻿using System;
using CorporateAPI.Dto;
using CorporateAPI.Exceptions;
using CorporateAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.BL
{
    public interface IStoreBL
    {
        Task<StoreModel> AddStore(StoreModel storeModel);
        Task<bool> UpdateStore(StoreModel storeModel);
        StoreModel GetStore(String storeClientId);
        StoreModel GetStore(int storeId);
        StoreDetailsDto GetStoreDetails(string StoreClientId);
        StoreDetailsDto GetStoreDetails(int storeId);
        StoreModel GetStore(int organizationId, string storeName);
        StoreModel GetStoreClientId(int organizationId, string storeClientId);
        List<StoreModel> GetStores(int organizationId);
    }
}
