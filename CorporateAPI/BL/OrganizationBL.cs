﻿using CorporateAPI.Exceptions;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorporateAPI.DatabaseService;
using MongoDB.Bson;

namespace CorporateAPI.BL
{
    public class OrganizationBL:IOrganizationBL
    {
        public IMongoDBService<OrganizationModel> organizationRepository { get; set; }
        public OrganizationBL(IMongoDBService<OrganizationModel> OrganizationRepository)
        {
            organizationRepository ??= new MongoDBService<OrganizationModel>();
        }

        /// <summary>
        /// Function to add the organization to the System
        /// </summary>
        /// <param name="requestModel">Api request Model/ Dto</param>
        /// <returns>Organization Model</returns>
        public async Task<OrganizationModel> AddOrganization(OrganizationRequestModel requestModel) {
            
            OrganizationModel organizationModel = new OrganizationModel()
            {
                Address=requestModel.Address,
                BussinessContact=requestModel.BussinessContact,
                Name=requestModel.Name,
                TechnicalContact=requestModel.TechnicalContact
            };
            return await this.AddOrganization(organizationModel);
        }

        /// <summary>
        /// Add the organization to system from the OrganizationModel
        /// </summary>
        /// <param name="model">Organization Model</param>
        /// <returns>null/ Organization Model</returns>
        public async Task<OrganizationModel> AddOrganization(OrganizationModel model) {
            model.OrganizationId = organizationRepository.FindAndIncrement("organization_id");
            bool IsInserted = await organizationRepository.InsertRecords(model, "organizationmaster");
            OrganizationModel organizationModelResult = null;
            if (IsInserted) { organizationModelResult = model; }
            return organizationModelResult;
        }

        /// <summary>
        /// Function used to get the organization using the Organization Id
        /// </summary>
        /// <param name="organizationId">Id of the Organization</param>
        /// <returns>Organization Model</returns>
        public OrganizationModel GetOrganization(int organizationId) {
            OrganizationModel store = null;
            List<OrganizationModel> Organizations = organizationRepository.Find("organizationmaster", x => x.OrganizationId == organizationId && x.IsActive == true).ToList();
            if (Organizations.Count > 0)
            {
                store = Organizations[0];
            }
            //else
            //{
            //    throw new OrganizationNotFoundException(organizationId);
            //}
            return store;
        }

        /// <summary>
        /// Function used to get all the organization 
        /// </summary>
        /// <returns>Organization Model</returns>
        public List<OrganizationModel> GetOrganization()
        {
            List<OrganizationModel> Organizations = organizationRepository.Find("organizationmaster",x=>x.IsActive==true).ToList();
            return Organizations;
        }
        /// <summary>
        /// Update the organization by Organization Id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> UpdateOrganization(OrganizationModel model)
        {
            bool IsUpdated = await organizationRepository.UpdateRecords(model, "organizationmaster", x => x.OrganizationId == model.OrganizationId);
            return IsUpdated;
        }

    }
}
