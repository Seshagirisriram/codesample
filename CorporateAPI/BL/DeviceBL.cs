﻿using CorporateAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using CorporateAPI.Models.Request;
using CorporateAPI.DatabaseService;
using CorporateAPI.Exceptions;


namespace CorporateAPI.BL
{
    public class DeviceBL:IDeviceBL
    {

        /// <summary>
        /// Static Logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(DeviceBL));
        private IMongoDBService<StoreModel> storeRepository { get; set; }
        private IMongoDBService<DeviceModel> deviceRepository { get; set; }
        public DeviceBL(IMongoDBService<StoreModel> StoreRepository, IMongoDBService<DeviceModel> DeviceRepository)
        {
            storeRepository ??= new MongoDBService<StoreModel>();
            deviceRepository ??= new MongoDBService<DeviceModel>();
        }
        /// <summary>
        /// Add Device Model
        /// </summary>
        /// <param name="deviceModel"></param>
        /// <returns></returns>
        public async Task<bool> AddDevice(DeviceModel deviceModel)
        {
            deviceModel.DeviceId = deviceRepository.FindAndIncrement("device_id");
            bool IsInserted = await deviceRepository.InsertRecords(deviceModel, "devicemaster");
            return IsInserted;

        }
        /// <summary>
        /// Get the device based on the Store Id
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns></returns>
        public List<DeviceModel> GetDevices(int storeId)
        {
            List<DeviceModel> Devices = deviceRepository.Find("devicemaster", x => x.StoreId == storeId && x.IsActive == true).ToList();
            return Devices;
        }

        /// <summary>
        /// Update Device 
        /// </summary>
        /// <param name="deviceModel"></param>
        /// <returns></returns>
        public async Task<bool> UpdateDevice(DeviceModel deviceModel)
        {
            bool IsUpdated = await deviceRepository.UpdateRecords(deviceModel, "devicemaster", x => x.Id == deviceModel.Id);
            return IsUpdated;

        }

        /// <summary>
        /// Get the device
        /// </summary>
        /// <param name="DeviceId">Id of the Device</param>
        /// <returns></returns>
        public async Task<DeviceModel> GetDevice(int DeviceId) {
            DeviceModel device = null;
            List<DeviceModel> Devices = deviceRepository.Find("devicemaster", x => x.DeviceId == DeviceId && x.IsActive == true).ToList();
            if (Devices.Count > 0)
            {
                device = Devices[0];
            }
            //else
            //{
            //    throw new DeviceNotFoundException(DeviceId);
            //}
            return device;
        }

        /// <summary>
        /// Get the device By Serial Number
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        public DeviceModel GetDeviceBySerialNumber(String serialNumber) {
            DeviceModel device = null;
            if (!String.IsNullOrEmpty(serialNumber))
            {
                List<DeviceModel> Devices = deviceRepository.Find("devicemaster", x => x.SerialNumber.ToLower() == serialNumber.ToLower() && x.IsActive == true).ToList();
                if (Devices.Count > 0)
                {
                    device = Devices[0];
                }

            }
            return device;
        }

        /// <summary>
        /// Used to add the device to the database
        /// </summary>
        /// <param name="deviceRequestModel"></param>
        /// <returns></returns>
        public async Task<DeviceModel> AddDevice(DeviceRequestModel deviceRequestModel) {
            bool IsSaved = true;
            StoreModel store = new StoreBL(storeRepository).GetStore(deviceRequestModel.StoreId);
            DeviceModel deviceModel = new DeviceModel()
            {
                IpAddress = deviceRequestModel.IpAddress,
                MacAddress = deviceRequestModel.MacAddress,
                SerialNumber=deviceRequestModel.SerialNumber,
                StoreId=store.StoreId,
                Type=deviceRequestModel.Type,
                
            };
            IsSaved=await this.AddDevice(deviceModel);
            return deviceModel;
        }

        /// <summary>
        /// Get the type By Serial Number
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        public DeviceModel GetTypeBySerialNumber(String serialNumber,String type)
        {
            DeviceModel device = null;
            if (!String.IsNullOrEmpty(serialNumber) && !String.IsNullOrEmpty(type))
            {
                List<DeviceModel> Devices = deviceRepository.Find("devicemaster", x =>(x.SerialNumber.ToLower() == serialNumber.ToLower() && x.Type.ToLower()== type.ToLower()) && (x.IsActive == true)).ToList();
                if (Devices.Count > 0)
                {
                    device = Devices[0];
                }
            }
            return device;
        }

    }
}
