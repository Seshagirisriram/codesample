﻿using CorporateAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net; 
namespace CorporateAPI.BL
{
    public class UsersBL
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(UsersBL));

        /// <summary>
        /// Get the user details from the username and password
        /// </summary>
        /// <param name="Username">Username of the user</param>
        /// <param name="Password">Password of the user</param>
        /// <returns>null/User details</returns>
        public UserModel GetUserDetails(String Username, String Password) {
            UserModel userModel = null;            
            MongoDBHelper<UserModel> mongoDBHelper = new MongoDBHelper<UserModel>();
            List<UserModel> userModels= mongoDBHelper.Find("users", x => x.Username.ToLower() == Username.ToLower() && x.Password == Password);
            if (userModels.Count == 1) {
                userModel = userModels[0];
            }
            return userModel;
        }
    }
}
