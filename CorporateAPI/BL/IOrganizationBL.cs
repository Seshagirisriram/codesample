﻿using System;
using CorporateAPI.Dto;
using CorporateAPI.Exceptions;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.BL
{
    public interface IOrganizationBL
    {
        Task<OrganizationModel> AddOrganization(OrganizationRequestModel requestModel);
        Task<OrganizationModel> AddOrganization(OrganizationModel model);
        OrganizationModel GetOrganization(int organizationId);
        Task<bool> UpdateOrganization(OrganizationModel model);
        List<OrganizationModel> GetOrganization();
    }
}
