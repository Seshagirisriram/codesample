﻿using CorporateAPI.Dto;
using CorporateAPI.Exceptions;
using CorporateAPI.Models;
using CorporateAPI.DatabaseService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.BL
{
    public class StoreBL:IStoreBL
    {
        public IMongoDBService<StoreModel> storeRepository { get; set; }
        private IMongoDBService<CameraModel> cameraRepository { get; set; }
        private IMongoDBService<ZoneModel> zoneRepository { get; set; }
        public StoreBL(IMongoDBService<StoreModel> StoreRepository)
        {
            cameraRepository ??= new MongoDBService<CameraModel>();
            storeRepository ??= new MongoDBService<StoreModel>();
            zoneRepository ??= new MongoDBService<ZoneModel>();
        }
        
        /// <summary>
        /// Used to insert Add Store
        /// </summary>
        /// <param name="storeModel">Add the store based on the </param>
        /// <returns></returns>
        public async Task<StoreModel> AddStore(StoreModel storeModel) {
            storeModel.StoreId = storeRepository.FindAndIncrement("store_id");
            bool IsInserted = await storeRepository.InsertRecords(storeModel,"storemaster");
            return storeModel;
        }

        /// <summary>
        /// Used to Update Store
        /// </summary>
        /// <param name="storeModel">Update the store based on the </param>
        /// <returns></returns>
        public async Task<bool> UpdateStore(StoreModel storeModel) {
            bool IsUpdated = await storeRepository.UpdateRecords(storeModel, "storemaster",x=>x.Id == storeModel.Id);
            return IsUpdated;
        }


        /// <summary>
        /// Get the store based on the Store Client Id
        /// </summary>
        /// <param name="storeClientId"></param>
        /// <exception cref="StoreNotFoundException"></exception>
        /// <returns></returns>
        public StoreModel GetStore(String storeClientId) {
            StoreModel store = null;
            List<StoreModel> Stores= storeRepository.Find("storemaster", x => x.StoreClientId.ToLower() == storeClientId.ToLower() && x.IsActive == true).ToList();

            if (Stores.Count > 0)
            {
                store = Stores[0];
            }
          
            return store;
        }

        /// <summary>
        /// Get the store based on the Store Client Id
        /// </summary>
        /// <param name="storeId"></param>
        /// <exception cref="StoreNotFoundException"></exception>
        /// <returns></returns>
        public StoreModel GetStore(int storeId)
        {
            StoreModel store = null;
            List<StoreModel> Stores = storeRepository.Find("storemaster", x => x.StoreId == storeId && x.IsActive == true).ToList();
            if (Stores.Count > 0)
            {
                store = Stores[0];
            }
            else
            {
                throw new StoreNotFoundException(storeId.ToString());
            }
            return store;
        }
        /// <summary>
        /// Get the store based on the Organization Id
        /// </summary>
        /// <param name="organizationId"></param>
        /// <returns></returns>
        public List<StoreModel> GetStores(int organizationId)
        {
            List<StoreModel> Stores = storeRepository.Find("storemaster", x => x.OrganizationId == organizationId && x.IsActive == true).ToList();
            return Stores;
        }
        /// <summary>
        /// Get the store details along with the camera details
        /// </summary>
        /// <param name="StoreClientId">Store Client Id</param>
        /// <returns></returns>
        public StoreDetailsDto GetStoreDetails(string StoreClientId) {
            StoreDetailsDto storeDetails = null;
            StoreModel store = this.GetStore(StoreClientId);
            if (store != null) {
                storeDetails = new StoreDetailsDto();
                String SerializedStore = JsonConvert.SerializeObject(store);
                storeDetails = JsonConvert.DeserializeObject<StoreDetailsDto>(SerializedStore);
                List<CameraDetailsDto> listOfCameraDetails = new List<CameraDetailsDto>();
                List<CameraModel> cameras = new CameraBL(cameraRepository,storeRepository).GetCameras(store.StoreId);
                foreach (CameraModel camera in cameras) {
                    List<ZoneModel> Zones = new ZonesBL(zoneRepository).GetZones(camera.CameraId);
                    String Serialized = JsonConvert.SerializeObject(camera);
                    CameraDetailsDto cameraDetails = JsonConvert.DeserializeObject<CameraDetailsDto>(Serialized);
                    cameraDetails.Zones = Zones;
                    listOfCameraDetails.Add(cameraDetails);
                }
                storeDetails.Cameras = listOfCameraDetails;
            }
            return storeDetails;
        }

        /// <summary>
        /// Get the store details along with the camera details
        /// </summary>
        /// <param name="storeId">Store Id</param>
        /// <returns></returns>
        public StoreDetailsDto GetStoreDetails(int storeId)
        {
            StoreDetailsDto storeDetails = new StoreDetailsDto();
            StoreModel store = this.GetStore(storeId);
            if (store != null)
            {
                String SerializedStore = JsonConvert.SerializeObject(store);
                storeDetails = JsonConvert.DeserializeObject<StoreDetailsDto>(SerializedStore);
                List<CameraDetailsDto> listOfCameraDetails = new List<CameraDetailsDto>();
                List<CameraModel> cameras = new CameraBL(cameraRepository, storeRepository).GetCameras(store.StoreId);
                foreach (CameraModel camera in cameras)
                {
                    List<ZoneModel> Zones = new ZonesBL(zoneRepository).GetZones(camera.CameraId);
                    String Serialized = JsonConvert.SerializeObject(camera);
                    CameraDetailsDto cameraDetails = JsonConvert.DeserializeObject<CameraDetailsDto>(Serialized);
                    cameraDetails.Zones = Zones;
                    listOfCameraDetails.Add(cameraDetails);
                }
                storeDetails.Cameras = listOfCameraDetails;
            }
            return storeDetails;
        }

        /// <summary>
        /// Get the store name based on the Organization Id
        /// </summary>
        /// <param name="organizationId"></param>
        /// <param name="storeName"></param>
        /// <exception cref="StoreNotFoundException"></exception>
        /// <returns></returns>
        public StoreModel GetStore(int organizationId,string storeName)
        {
            StoreModel store = null;
            List<StoreModel> Stores = storeRepository.Find("storemaster", x => (x.OrganizationId == organizationId && x.Name == storeName) && (x.IsActive == true)).ToList();
            if (Stores.Count > 0)
            {
                store = Stores[0];
            }
            
            return store;
        }

        /// <summary>
        /// Get the store client id based on the Organization Id
        /// </summary>
        /// <param name="organizationId"></param>
        /// <param name="storeName"></param>
        /// <exception cref="StoreNotFoundException"></exception>
        /// <returns></returns>
        public StoreModel GetStoreClientId(int organizationId, string storeClientId)
        {
            StoreModel store = null;
            List<StoreModel> Stores = storeRepository.Find("storemaster", x =>( x.OrganizationId == organizationId && x.StoreClientId.ToLower() == storeClientId.ToLower()) && (x.IsActive == true)).ToList();
            if (Stores.Count > 0)
            {
                store = Stores[0];
            }
           
            return store;
        }
    }
}
