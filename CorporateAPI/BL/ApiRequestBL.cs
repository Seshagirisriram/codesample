﻿namespace CorporateAPI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CorporateAPI.DatabaseService;
    using CorporateAPI.Models;
    using CorporateAPI.Models.Request;

    /// <summary>
    /// Deals with what??
    /// </summary>
    public class ApiRequestBL
    {
        private IMongoDBService<StoreApiRequestMaster> mongoDBHelper;

        /// <summary>
        /// Gets what ? Document this... 
        /// </summary>
        public IMongoDBService<StoreApiRequestMaster> StoreApiRequestMaster
        {
            get
            {
                if (this.mongoDBHelper == null)
                {
                    this.mongoDBHelper = new MongoDBService<StoreApiRequestMaster>();
                }
                return this.mongoDBHelper;
            }
        }

        /// <summary>
        /// Add the Api Request Master into the database
        /// </summary>
        /// <param name="requestModel">RequestModel</param>
        /// <returns>Task</returns>
        public async Task<StoreApiRequestMaster> AddRequests(ApiRequestModel requestModel)
        {
            if (requestModel != null)
            {
                StoreApiRequestMaster storeApis = new StoreApiRequestMaster();
                storeApis.StoreId = requestModel.StoreId;
                storeApis.ApiRequests = requestModel.ApiRequestEvents;
                bool isInserted = await this.mongoDBHelper.InsertRecords(storeApis, "storeapirequestmapping").ConfigureAwait(false);
                return storeApis;
            }
            else
            {
                return null;
            }
        }
    }
}
