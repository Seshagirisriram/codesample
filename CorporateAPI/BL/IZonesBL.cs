﻿using System;
using CorporateAPI.Dto;
using CorporateAPI.Exceptions;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.BL
{
    public interface IZonesBL
    {
        Task<ZoneModel> AddZone(ZoneRequestModel zoneRequestModel);
        List<ZoneModel> GetZones(int cameraId);
        ZoneModel GetZone(int zoneId);
        Task<bool> UpdateZone(ZoneModel model);
        ZoneModel GetZone(string zone, int cameraId);
       
    }
}
