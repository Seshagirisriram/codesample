﻿using CorporateAPI.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;

namespace CorporateAPI.BL
{
    public class SummaryReportBL
    {
        private static ILog Log = LogManager.GetLogger(typeof(SummaryReportBL));

        public List<SummaryReportDto> GetReport(SummaryReportRequestModel requestModel) {
            List<SummaryReportDto> SummaryReportData = new List<SummaryReportDto>();
            try
            {
                MongoDBHelper<MongoAiviData> mongoDBHelper = new MongoDBHelper<MongoAiviData>();
                DateTime startDateTime = ((DateTime)requestModel.FromDate).Date;
                DateTime endDateTime = ((DateTime)requestModel.ToDate).Date.AddDays(1);
                var StartDateWithOffset = new DateTime(startDateTime.Year, startDateTime.Month, startDateTime.Day,
                 TimeZoneInfo.Local.BaseUtcOffset.Hours, TimeZoneInfo.Local.BaseUtcOffset.Minutes, 0);
                var EndDateWithOffset = new DateTime(endDateTime.Year, endDateTime.Month, endDateTime.Day,
                  TimeZoneInfo.Local.BaseUtcOffset.Hours, TimeZoneInfo.Local.BaseUtcOffset.Minutes, 0);
                List<MongoAiviData> mongoAiviDatas = mongoDBHelper.FetchAiviDataDateSearch(StartDateWithOffset, EndDateWithOffset);

                foreach (int storeId in requestModel.StoreIds) {
                    SummaryReportDto Dto = new SummaryReportDto();
                    Dto.StoreId = storeId;
                    Dto.FootFall = new List<FootFallDetailsDto>();
                    List<MongoAiviData> StoreWiseData = mongoAiviDatas.Where(x => x.StoreId == storeId).ToList();
                    List<DateTime> DistinctDate = StoreWiseData.Select(x => x.TimeStamp.Date).ToList();
                    var DateWiseGrouping = from storeWise in StoreWiseData group storeWise by storeWise.TimeStamp.Date
                                           into egroup select new FootFallDetailsDto() {
                                               Date = egroup.Key.Date.ToString("yyyy-MM-dd"),
                                               TotalIn = egroup.Where(x => x.EventCode == "PEZ").Count(),
                                               TotalOut = egroup.Where(x => x.EventCode == "PLZ").Count(),
                                               Min = 1,
                                               Max = egroup.Max(x => x.CurrentInStoreCount),
                                               Allowed = 15,
                                               Average = (int)egroup.Average(x => x.CurrentInStoreCount)
                                           };
                    foreach (var grouped in DateWiseGrouping) {
                        Dto.FootFall.Add(grouped);
                    }
                    SummaryReportData.Add(Dto);

                }
                
            }
            catch (Exception e) {
                // Sriram: Throw an exception in the middle of the loop above .. What is returned.. 
                Log.Error(e.StackTrace);
            }
            return SummaryReportData;
        }
    }
}
