﻿using System;
using CorporateAPI.Dto;
using CorporateAPI.Exceptions;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.BL
{
    public interface ICameraBL
    {
            Task<CameraModel> AddCamera(CameraRequestModel cameraRequestModel);
            List<CameraModel> GetCameras(int storeId);
            CameraModel GetCamera(int cameraId);
            CameraModel GetIpAddressByStoreId(string cameraIp, int storeId);
            CameraModel GetIpAddressByCameraId(string cameraIp, int cameraId);
            CameraModel GetCameraByStoreClientId(CameraRequestModel cameraRequestModel);
            Task<bool> UpdateCamera(CameraModel model);
    }
}
