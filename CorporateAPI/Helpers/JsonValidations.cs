﻿using System;
using System.Linq;
using CorporateAPI.BL;
using CorporateAPI.Models;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using CorporateAPI.DatabaseService;

namespace CorporateAPI.Helpers
{
    public class JsonValidations
    {
        public ICameraBL cameraBL { get; set; }
        public IZonesBL zoneBL { get; set; }
        private IMongoDBService<StoreModel> storeRepository { get; set; }
        private IMongoDBService<CameraModel> cameraRepository { get; set; }
        private IMongoDBService<ZoneModel> zoneRepository { get; set; }
        public void ValidateJson(ZoneListModel jsonResponse,int CameraId)
        {
            cameraBL = new CameraBL(cameraRepository, storeRepository);
            zoneBL = new ZonesBL(zoneRepository);

            // Check for required fields set by values in JSON
            if (String.IsNullOrEmpty(jsonResponse.camera_name))
            {
                throw new JsonException("Camera name is required");
            }
            if (String.IsNullOrEmpty(jsonResponse.camera_ip))
            {
                throw new JsonException("Camera ip is required");
            }
            if (String.IsNullOrEmpty(jsonResponse.annotation_resolution))
            {
                throw new JsonException("Resolution is required");
            }
            foreach (var zone in jsonResponse.zones)
            {
                if (String.IsNullOrEmpty(zone.name))
                {
                    throw new JsonException("Name is required");
                }
                if (String.IsNullOrEmpty(zone.type))
                {
                    throw new JsonException("Type is required");
                }
                if (String.IsNullOrEmpty(zone.shape))
                {
                    throw new JsonException("Shape is required");
                }
                if (String.IsNullOrEmpty(zone.coordinates))
                {
                    throw new JsonException("Coordinates is required");
                }
                if (String.IsNullOrEmpty(zone.version))
                {
                    throw new JsonException("Version is required");
                }
                if (String.IsNullOrEmpty(zone.marking_datetime))
                {
                    throw new JsonException("Marking datetime is required");
                }
                if (!String.IsNullOrEmpty(zone.marking_datetime))
                {
                    Regex datePattern = new Regex(@"^(\d{4}-\d{2}-\d{2}T\d{2}\:\d{2}\:\d{2}\.\d+Z)|(\d{4}-\d{2}-\d{2}T\d{2}\:\d{2}\:\d{2}Z)$");
                    if (!datePattern.IsMatch(zone.marking_datetime))
                        throw new JsonException("Invalid date format");
                }
            }
            //ROI
            var zoneROIList = (from s in jsonResponse.zones
                               where s.name == "ROI"
                               select new
                               {
                                   s.coordinates,
                                   s.name,
                                   s.type,
                                   s.shape

                               }).ToList();

            string resolution = jsonResponse.annotation_resolution;
            string[] resolutionList = resolution.Split('x');

            int xresolution = Convert.ToInt32(resolutionList[0]);
            int yresolution = Convert.ToInt32(resolutionList[1]);

            bool IsROIExists = (zoneROIList.Count > 0);
            bool IsROITypeExists = zoneROIList.Any(x => (x.type == "ROI"));
            bool IsPolygonExists = zoneROIList.Any(x => (x.shape.ToLower() == "polygon"));
            bool IsXCoordinateLessThanZero = false;
            bool IsYCoordinateLessThanZero = false;
            bool IsXCoordinateGreaterThanAnnotation = false;
            bool IsYCoordinateGreaterThanAnnotation = false;
            ZoneModel IsZoneNameExists = null;
            string roiCoordinates = zoneROIList[0].coordinates;

            string[] roiList = roiCoordinates.Split(',');
            int countROICoordinates = roiList.Length;
            foreach (string x in roiList)
            {


                object[] roiSide = x.ToString().Split(":");
                int xcoordinate = Convert.ToInt32(roiSide[0]);
                int ycoordinate = Convert.ToInt32(roiSide[1]);
                if (xcoordinate < 0)
                    IsXCoordinateLessThanZero = true;

                if (ycoordinate < 0)
                    IsYCoordinateLessThanZero = true;

                if (xcoordinate > xresolution)
                    IsXCoordinateGreaterThanAnnotation = true;

                if (ycoordinate > yresolution)
                    IsYCoordinateGreaterThanAnnotation = true;

            }

            //Zones inside the ROI
            var zonesInROIList = (from s in jsonResponse.zones
                                  where s.name != "ROI"
                                  select new
                                  {
                                      s.coordinates,
                                      s.name,
                                      s.type,
                                      s.shape

                                  }).ToList();

            int countZonesInROIList = zonesInROIList.Count;

            bool IsInROITypeZoneExists = false;
            bool IsInROIPolygonExists = false;
            bool IsInROIXCoordinateLessThanZero = false;
            bool IsInROIYCoordinateLessThanZero = false;
            bool IsInROIXCoordinateGreaterThanAnnotation = false;
            bool IsInROIYCoordinateGreaterThanAnnotation = false;
            int countInROICoordinates = 0;
            foreach (var zone in zonesInROIList)
            {

                IsZoneNameExists = zoneBL.GetZone(zone.name, CameraId);

                IsInROITypeZoneExists = (zone.type == "Zone");
                IsInROIPolygonExists = (zone.shape.ToLower() == "polygon");


                string[] roiInList = zone.coordinates.Split(',');
                countInROICoordinates = roiInList.Length;
                foreach (string x in roiInList)
                {


                    object[] roiSide = x.ToString().Split(":");
                    int xcoordinate = Convert.ToInt32(roiSide[0]);
                    int ycoordinate = Convert.ToInt32(roiSide[1]);
                    if (xcoordinate < 0)
                        IsInROIXCoordinateLessThanZero = true;

                    if (ycoordinate < 0)
                        IsInROIYCoordinateLessThanZero = true;

                    if (xcoordinate > xresolution)
                        IsInROIXCoordinateGreaterThanAnnotation = true;

                    if (ycoordinate > yresolution)
                        IsInROIYCoordinateGreaterThanAnnotation = true;

                }
            }

            bool IsValid = (IsROIExists == false) ? throw new JsonException("ROI doesn't exists") :
            (IsROITypeExists == false) ? throw new JsonException("ROI type doesn't exists") :
            (IsPolygonExists == false) ? throw new JsonException("Shape should be 'polygon'") :
            (countROICoordinates != 4) ? throw new JsonException("Co-ordinates of ROI doesn't contains 4 set of points") :
            (IsXCoordinateLessThanZero == true) ? throw new JsonException("X co-ordinates of ROI is less than zero") :
            (IsYCoordinateLessThanZero == true) ? throw new JsonException("Y co-ordinates of ROI is less than zero") :
            (IsXCoordinateGreaterThanAnnotation == true) ? throw new JsonException("X co-ordinates of ROI is greater than annotation resolution") :
            (IsYCoordinateGreaterThanAnnotation == true) ? throw new JsonException("Y co-ordinates of ROI is greater than annotation resolution") :
            (countZonesInROIList == 0) ? throw new JsonException("At least one zone (additional to ROI) must be defined") :
            (IsZoneNameExists != null) ? throw new JsonException("Zone name must be unique") :
            (IsInROITypeZoneExists == false) ? throw new JsonException("ROI type doesn't exists") :
            (IsInROIPolygonExists == false) ? throw new JsonException("Shape should be 'polygon'") :
            (countInROICoordinates != 4) ? throw new JsonException("Co-ordinates of zone doesn't contains 4 set of points") :
            (IsInROIXCoordinateLessThanZero == true) ? throw new JsonException("X co-ordinates of zone is less than zero") :
            (IsInROIYCoordinateLessThanZero == true) ? throw new JsonException("Y co-ordinates of zone is less than zero") :
            (IsInROIXCoordinateGreaterThanAnnotation == true) ? throw new JsonException("X co-ordinates of zone is greater than annotation resolution") :
            (IsInROIYCoordinateGreaterThanAnnotation == true) ? throw new JsonException("Y co-ordinates of zone is greater than annotation resolution"):  true ; 
        }
    }
}