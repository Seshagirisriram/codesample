﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI
{
    public static class Globals
    {
        public static int StoreReportDefaultInterval;
        public static TimeSpan StoreReportDefaultStartTime;
        public static TimeSpan StoreReportDefaultEndTime;

        // This is a comment by Sriram.. 
        public static void Load() {
            _ = Int32.TryParse(ConfigurationManager.AppSettings["StoreReportDefaultInterval"],out StoreReportDefaultInterval);
            _ = TimeSpan.TryParse(ConfigurationManager.AppSettings["StoreReportDefaultStartTime"], out StoreReportDefaultStartTime);
            _ = TimeSpan.TryParse(ConfigurationManager.AppSettings["StoreReportDefaultEndTime"], out StoreReportDefaultStartTime);

        }

    }
}
