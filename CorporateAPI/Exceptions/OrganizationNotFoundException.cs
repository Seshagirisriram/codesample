﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Exceptions
{
    public class OrganizationNotFoundException : Exception
    {
        public OrganizationNotFoundException() { }

        public OrganizationNotFoundException(int organizationIdentifier)
            : base(String.Format("Organization doesn't exist for the provided organizationId: {0}", organizationIdentifier))
        { }
    }
}
