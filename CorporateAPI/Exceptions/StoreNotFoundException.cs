﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Exceptions
{
    public class StoreNotFoundException : Exception
    {
        public StoreNotFoundException(){}

        public StoreNotFoundException(string storeIdentifier)
            : base(String.Format("No store identified under the mentioned store identifier : {0}", storeIdentifier))
        {}
    }
}
