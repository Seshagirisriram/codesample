﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporateAPI.Exceptions
{
    public class DeviceNotFoundException : Exception
    {
        public DeviceNotFoundException() { }

        public DeviceNotFoundException(int deviceIdentifier)
            : base(String.Format("Device doesn't exist for the provided deviceId: {0}", deviceIdentifier))
        { }
    }
}
