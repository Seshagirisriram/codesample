﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorporateAPI.Models;
using CorporateAPI.Models.Response;
using CorporateAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using log4net;
using Castle.Core.Logging;

namespace CorporateAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
     public class UsersController : ControllerBase
    {
        private IUserService _userService;
        /// <summary>
        /// Static Logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(SummaryReportController));
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }
        /// <summary>
        /// Authenticate an user by username & password
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]AuthenticateModel model)
        {
            IActionResult response = null;
            AuthenticationResponseModel ResponseModel = null; 
            try
            {
                ResponseModel = new AuthenticationResponseModel();
                var user = _userService.Authenticate(model.Username, model.Password);

                if (user != null)
                {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARFA-S0203", Text = "Authentication successful" } };
                    ResponseModel.ResultCode = "OK";
                    ResponseModel.Messages = messages;
                    ResponseModel.AuthenticationToken = user.Token;
                    response = Ok(ResponseModel);
                    Log.Info($"User: {model.Username} has been successfully authenticated.");
                }
                else
                {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARFA-E0203", Text = "Failed to authenticate" } };
                    ResponseModel.ResultCode = "ERR";
                    ResponseModel.Messages = messages;
                    response = Ok(ResponseModel);
                    Log.Error($"User: {model.Username} could not be authenticated.");

                }
                return response;
            }
            catch (Exception e)
            {
                Log.Error($"Authenticate(): error in authenticating user: {model.Username}");
                Log.Error($"Error Message: {e.Message}"); 
                ResponseModel = new AuthenticationResponseModel();
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARFA-E0203", Text = "Failed to authenticate" } };
                Response.StatusCode = 400; 
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = Ok(ResponseModel);
                return response; 
            }
        }

    }
}