﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft;
using log4net;
using Microsoft.AspNetCore.Authorization;

namespace CorporateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventApiSimulatorController : ControllerBase
    {

        /// <summary>
        /// Static Logger
        /// </summary>
        public static ILog Log = LogManager.GetLogger(typeof(EventApiSimulatorController));

        [AllowAnonymous]
        [HttpPost("data")]
        public bool PostData(Dictionary<String, String> requestModel) {
            Log.Info(requestModel);   
            return true;
        }

        [AllowAnonymous]
        [HttpPost("event")]
        public bool PostEvent(Dictionary<String, String> requestModel)
        {
            Log.Info(requestModel);
            return true;
        }
    }
}