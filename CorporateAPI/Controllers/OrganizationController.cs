﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorporateAPI.Models.Request;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using log4net;
using CorporateAPI.Models.Response;
using CorporateAPI.Models;
using CorporateAPI.BL;
using CorporateAPI.Exceptions;
using CorporateAPI.DatabaseService;

namespace CorporateAPI.Controllers
{
    /// <summary>
    /// 
    /// Organization Controller 
    
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizationController : ControllerBase
    {
        /// <summary>
        /// Static Logger
        /// </summary>
        public static readonly ILog Log = LogManager.GetLogger(typeof(OrganizationController));
        private IMongoDBService<OrganizationModel> organizationRepository { get; set; }
        public IOrganizationBL organizationBL { get; set; }

        /// <summary>
        /// Add the organization
        /// </summary>
        /// <param name="requestModel">request Model</param>
        /// <returns>Created/Error Response</returns>
        [HttpPost("add")]
        public IActionResult AddOrganization(OrganizationRequestModel requestModel) {
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            organizationBL = new OrganizationBL(organizationRepository);
            try
            {
                OrganizationModel organizationModel = organizationBL.AddOrganization(requestModel).Result;
                if (organizationModel != null)
                {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0501", Text = "Organization Added Successfully" } };
                    responseModel.ResultCode = "OK";
                    responseModel.Id = organizationModel.OrganizationId;
                    responseModel.Messages = messages;
                    response = Created($"/api/organization/get/{organizationModel.OrganizationId}", responseModel);
                }
                else
                {
                    Log.Error("AddOrganization(): null organizationModel passed"); 
                    List <CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0502", Text = "Failed to process the request" } };
                    responseModel.ResultCode = "ERR";
                    responseModel.Messages = messages;
                    response = Ok(responseModel);
                }
            }
            catch (Exception e)
            {
                Log.Error($"Failed to add the organization due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0502", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            return response;
        }

        /// <summary>
        /// Get the Organization details based on the OrganizationId 
        /// </summary>
        /// <param name="organizationId">Id of the Organization</param>
        /// <returns></returns>
        [HttpGet("get/{organizationId}")]
        public IActionResult GetOrganization(int organizationId) {
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            organizationBL = new OrganizationBL(organizationRepository);
            try
            {
                OrganizationModel organizationModel = organizationBL.GetOrganization(organizationId);
                response = Ok(organizationModel);
            }
            catch (OrganizationNotFoundException e) {
                Log.Error($"Failed to add the organization due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0503", Text = e.Message } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            catch (Exception e)
            {
                Log.Error($"Failed to add the organization due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0502", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            return response;
        }
        /// <summary>
        /// Get all the Organization 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get")]
        public IActionResult GetOrganization()
        {
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            organizationBL = new OrganizationBL(organizationRepository);
            try
            {
                List<OrganizationModel> organizationModel = organizationBL.GetOrganization();
                response = Ok(organizationModel);
            }
            catch (Exception e)
            {
                Log.Error($"Failed to get the organization due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0502", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            return response;
        }
        /// <summary>
        /// Update an existing organization
        /// </summary>
        /// <returns></returns>
        [HttpPost("update")]
        public IActionResult UpdateOrganization(OrganizationRequestModel requestModel)
        {
            IActionResult response = null;
            BaseResponseModel ResponseModel = new BaseResponseModel();
            organizationBL = new OrganizationBL(organizationRepository);
            try
            {
                OrganizationModel IsExists = null;
                try
                {
                    IsExists = organizationBL.GetOrganization(requestModel.OrganizationId);
                }
                catch (Exception e)
                {
                    // pretty much redundant and adds no value to logging chain... 
                    Log.Error(e.Message);
                }

                if (IsExists != null)
                {
                    OrganizationModel organizationModel = new OrganizationModel()
                    {
                        OrganizationId=IsExists.OrganizationId,
                        Id = IsExists.Id,
                        Name = requestModel.Name,
                        Address=requestModel.Address,
                        BussinessContact = requestModel.BussinessContact,
                        TechnicalContact = requestModel.TechnicalContact
                    };
                    bool OrganizationUpdated = organizationBL.UpdateOrganization(organizationModel).Result;
                    if (OrganizationUpdated)
                    {
                        List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0401", Text = "Organization updated successfully" } };
                        ResponseModel.Id = IsExists.OrganizationId;
                        ResponseModel.ResultCode = "OK";
                        ResponseModel.Messages = messages;

                    }
                    else
                    {
                        List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                        ResponseModel.ResultCode = "ERR";
                        ResponseModel.Messages = messages;

                    }
                    // Sriram: if update fails, should this be OK
                    response = Ok(ResponseModel);

                }
                else
                {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Organization doen't exitsts" } };
                    ResponseModel.ResultCode = "ERR";
                    ResponseModel.Messages = messages;
                    response = Ok(ResponseModel);

                }

            }
            catch (Exception e)
            {
                Log.Error($"Failed to update the organization due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }
        /// <summary>
        /// Deactivate an existing organization
        /// </summary>
        /// <returns></returns>
        [HttpPost("deactivate/{organizationId}")]
        public IActionResult DeactivateOrganization(int organizationId)
        {
            return this.ToggleActivation(organizationId, false);

        }
        [NonAction]
        public IActionResult ToggleActivation(int organizationId, bool flag)
        {
            IActionResult response = null;
            BaseResponseModel ResponseModel = new BaseResponseModel();
            organizationBL = new OrganizationBL(organizationRepository);
            try
            {
                OrganizationModel ExistingOrganization = organizationBL.GetOrganization(organizationId);
                if (ExistingOrganization != null)
                {
                    ExistingOrganization.IsActive = flag;
                    bool OrganizationUpdated = organizationBL.UpdateOrganization(ExistingOrganization).Result;
                    if (OrganizationUpdated)
                    {
                        List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0401", Text = "Completed successfully" } };
                        ResponseModel.ResultCode = "OK";
                        ResponseModel.Id = ExistingOrganization.OrganizationId;
                        ResponseModel.Messages = messages;
                    }
                    else
                    {
                        List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                        ResponseModel.ResultCode = "ERR";
                        ResponseModel.Messages = messages;
                    }

                    response = Ok(ResponseModel);
                }
                else
                {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Organization doesn't exitsts" } };
                    ResponseModel.ResultCode = "ERR";
                    ResponseModel.Messages = messages;
                    response = Ok(ResponseModel);

                }

            }
            catch (Exception e)
            {
                // Sigh.. more copy/paste error..
                Log.Error($"Failed to toggle activation for Organization: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }

    }
}