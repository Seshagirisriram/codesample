﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorporateAPI.BL;
using CorporateAPI.Helpers;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using CorporateAPI.Models.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using log4net;
using CorporateAPI.ModelBinders;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.FileProviders;
using CorporateAPI.DatabaseService;

namespace CorporateAPI.Controllers
{
    /// <summary>
    /// PeopleCount Controller 
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleCountController : ControllerBase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(PeopleCountController));
        public IPeopleCountBL peopleCountBL { get; set; }
        private IMongoDBService<PeopleCountRequestModel> peopleRepository { get; set; }
        /// <summary>
        /// Get the count of people in a store 
        /// </summary>
        /// <returns></returns>
        [HttpPost("count")]
        public IActionResult GetPeopleCount(PeopleCountRequestModel requestModel)
        {
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            peopleCountBL = new PeopleCountBL(peopleRepository);
            // Sriram: inconsistent across all API... 
            try
            {
                PeopleCountResponseModel peopleCountResponseModel = peopleCountBL.GetPeopleCountDetails(requestModel);
                response = Ok(peopleCountResponseModel);
            }
            catch (Exception e)
            {
                Log.Error($"Failed to get the camera details due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0502", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            return response;
        }
    }
}