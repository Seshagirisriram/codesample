﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Configuration;
using MongoDB.Driver;
using MongoDB.Bson;
using System.IO;
using Newtonsoft.Json;
using MongoDB.Bson.Serialization.Attributes;

namespace CorporateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CorporateDashboardController : ControllerBase
    {
        [HttpPost]
        public CorporateDashboardResponseModel Post(CorporateDashboardRequestModel request)
        {
            var response = new CorporateDashboardResponseModel();
            bool dirty = false;
            if (request.Stores.Count == 0) response.Messages.Add(new Message { Code = "ARFA-E0000", Text = "Invalid request parameters" });
            response.StoresState = StoresState.GetState(request.Stores);
            return response;
        }
    }

    public class CorporateDashboardRequestModel
    {
        public List<int> Stores { get; set; }

    }

    public partial class CorporateDashboardResponseModel
    {
        private List<StoresState> storesState = new List<StoresState>();
        private List<Message> messages = new List<Message>();

        public List<StoresState> StoresState { get { return storesState; } set { storesState = value; } }
        public List<Message> Messages { get { return messages; } }
    }

    public partial class Message
    {
        public string Code { get; set; }
        public string Text { get; set; }
    }

    public partial class StoresState
    {
        public static object ConfigurationManager { get; private set; }
        public long StoreId { get; set; }
        public long TotalIn { get; set; }
        public long TotalOut { get; set; }
        public long CurrentInStoreCount { get; set; }
        public long AllowedInStoreCount { get; set; }
        public List<Alert> Alerts { get; set; }
        public List<Camera> Cameras { get; set; }
        public string ResponseGenerationTime { get; set; }

        internal static List<StoresState> GetState(List<int> stores)
        {
            //var constr = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
            //var client = new MongoClient(constr);
            var client = new MongoDBHelper<CorporateDashboardController>().getClientFromConfig();
            var clientDB = "cicdswarak"; //System.Configuration.ConfigurationManager.AppSettings["MongoClientDatabaseName"];
            var database = client.GetDatabase(clientDB);
            var collection = database.GetCollection<MongoAiviData>("aividata");
            var filter = Builders<MongoAiviData>.Filter.In("StoreId", stores);
            var docs = collection.Find(filter);
            var list = new List<StoresState>();
            foreach (var doc in docs.ToList())
            {
                var isNew = false;
                var store = list.Find(s => s.StoreId == doc.StoreId);
                if (store == null) { store = new StoresState(); isNew = true; }

                store.StoreId = doc.StoreId;
                if (doc.EventCode == "PEZ") store.TotalIn += 1;
                if (doc.EventCode == "PLZ") store.TotalOut += 1;
                store.CurrentInStoreCount = store.TotalIn - store.TotalOut;
                store.AllowedInStoreCount = GetAllowedCount(store.StoreId);
                store.ResponseGenerationTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

                if (isNew) { list.Add(store); }
            }

            return list;

            int GetAllowedCount(long id)
            {
                var setttings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText("settings.json"));
                var count = setttings.AllowedCount.Find(x => x.StoreId == id).PeopleCount;
                return count;
            }

        }
    }

    public partial class Settings
    {
        public List<AllowedCount> AllowedCount { get; set; }
    }

    public partial class AllowedCount
    {
        public int StoreId { get; set; }
        public int PeopleCount { get; set; }
    }



    public partial class Alert
    {
        public string AlertName { get; set; }
        public string AlertMetadata { get; set; }
    }

    public partial class Camera
    {
        public string CameraId { get; set; }
        public string CameraStatus { get; set; }
    }

    public partial class MongoAiviData
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string EventCode { get; set; }
        public long FrameID { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
        public Guid ObjectID { get; set; }
        public string Category { get; set; }
        public SpatialData SpatialData { get; set; }
        public EventData EventData { get; set; }
        public string Zone { get; set; }
        public long StoreId { get; set; }
        public long CameraId { get; set; }
    }

    public partial class EventData
    {
        public object Gender { get; set; }
        public object AgeString { get; set; }
        public object Ethnicity { get; set; }
        public object Zone { get; set; }
    }

    public partial class SpatialData
    {
        public string Confidence { get; set; }
        public long X { get; set; }
        public long Y { get; set; }
        public long H { get; set; }
        public long W { get; set; }
    }

}