﻿using System;
using System.Collections.Generic;
using CorporateAPI.BL;
using CorporateAPI.DatabaseService;
using CorporateAPI.Dto;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using CorporateAPI.Models.Response;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CorporateAPI.Controllers
{
    /// <summary>
    /// Store controller
    /// </summary>
    /// <returns></returns>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class StoreController : ControllerBase
    {
        /// <summary>
        /// Static Logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(StoreController));
        public IStoreBL storeBL { get; set; }
        private IMongoDBService<StoreModel> storeRepository { get; set; }

        private IMongoDBService<OrganizationModel> organizationRepository { get; set; }

        /// <summary>
        /// Find a store by store client Id
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("{StoreClientId}")]
        public IActionResult Get(String StoreClientId) {
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            try
            {
                StoreDetailsDto storeDetails = new StoreBL(storeRepository).GetStoreDetails(StoreClientId);
                response = Ok(storeDetails);
            }
            catch (Exception e)
            {
                Log.Error($"Failed to get the stores due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0502", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            return response;
        }
        /// <summary>
        /// Find a store by store id
        /// </summary>
        /// <returns></returns>
        [HttpGet("get/{storeId}")]
        public IActionResult Get(int storeId)
        {
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            try
            {
                StoreDetailsDto storeDetails = new StoreBL(storeRepository).GetStoreDetails(storeId);
                response = Ok(storeDetails);
            }
            catch (Exception e)
            {
                Log.Error($"Failed to get the stores due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0502", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            return response;
        }
        /// <summary>
        /// Get all the stores in an organization 
        /// </summary>
        /// <returns></returns>
        [HttpGet("get/Org={OrganizationId}")]
        public IActionResult GetStores([FromRoute] int OrganizationId)
        {
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            storeBL = new StoreBL(storeRepository);
            try
            {
                List<StoreModel> storeModel = storeBL.GetStores(OrganizationId);
                response = Ok(storeModel);
            }
            catch (Exception e)
            {
                Log.Error($"Failed to get the stores due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0502", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            return response;
        }
        /// <summary>
        /// Add a store apis
        /// </summary>
        /// <returns></returns>
        [HttpPost("apis/add")]
        public IActionResult AddApis(ApiRequestModel requestModel) {
            // Sriram: rename the above to a more meaninful name not just AddApis
            ApiRequestBL apiRequestBL = new ApiRequestBL();
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            StoreModel IsExists = null;
            storeBL = new StoreBL(storeRepository);
            try
            {
                IsExists = storeBL.GetStore(requestModel.StoreClientId);
            }
            catch (Exception e)
            {
                Log.Error($"Failed to get the stores due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0502", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            if (IsExists != null)
            {
                StoreApiRequestMaster addedEntry = apiRequestBL.AddRequests(requestModel).Result;
                if (addedEntry != null)
                {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0401", Text = "Store Apis added successfully" } };
                    responseModel.ResultCode = "OK";
                    responseModel.Messages = messages;
                    response = Created("", responseModel);
                }
                else
                {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                    responseModel.ResultCode = "ERR";
                    responseModel.Messages = messages;
                    response = Ok(responseModel);
                }
            }
            else {
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Store doen't exitsts" } };
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = Ok(responseModel);
            }
            return response;

        }
        /// <summary>
        /// Add a store to an organization
        /// </summary>
        /// <returns></returns>
        [HttpPost("add")]
        public IActionResult ProcessAddStore(StoreRequestModel storeRequestModel) {
            
            IActionResult response = null;
            StoreResponseModel ResponseModel = new StoreResponseModel();
            StoreModel IsStoreNameExists = null;
            StoreModel IsStoreClientIdExists = null;
            OrganizationModel IsOrganizationExists = null;
            storeBL = new StoreBL(storeRepository);
            OrganizationBL organizationBL = new OrganizationBL(organizationRepository);
          
            try
            {
                StoreModel storeModel = new StoreModel() { Name=storeRequestModel.Name, 
                    StoreClientId =storeRequestModel.StoreClientId,TimeZone=storeRequestModel.TimeZone,
                    StoreOpenTime = storeRequestModel.StoreOpenTime , StoreCloseTime=storeRequestModel.StoreCloseTime,
                    AllowedPeople = storeRequestModel.AllowedPeople,
                    GeoLocation= storeRequestModel.GeoLocation,
                    BussinessContact = storeRequestModel.BussinessContact,
                    TechnicalContact = storeRequestModel.TechnicalContact,
                    OrganizationId=storeRequestModel.OrganizationId,
                    TagId=storeRequestModel.TagId

                };
                IsStoreNameExists = storeBL.GetStore(storeRequestModel.OrganizationId,storeRequestModel.Name);
                IsStoreClientIdExists = storeBL.GetStoreClientId(storeRequestModel.OrganizationId, storeRequestModel.StoreClientId);
                IsOrganizationExists = organizationBL.GetOrganization(storeRequestModel.OrganizationId);

                string StoreNameExists = "Store name exists";
                string StoreClientIdExistsMessage = "Store Client Id exists";
                string OrganizationNotExistsMessage = "Organization doesn't exists";

                //IActionResult storeActionResult = (IsStoreNameExists != null) ? DisplayMessage(StoreNameExists) :
                //          (IsStoreClientIdExists != null) ? DisplayMessage(StoreClientIdExistsMessage) :
                //          (IsOrganizationExists == null) ? DisplayMessage(OrganizationNotExistsMessage) : AddStore(storeModel);
                IActionResult storeActionResult = (IsStoreNameExists != null) ? DisplayMessage(StoreNameExists) :
                                                  ((IsStoreClientIdExists != null) ? DisplayMessage(StoreClientIdExistsMessage) :
                                                 ((IsOrganizationExists == null) ? DisplayMessage(OrganizationNotExistsMessage) : AddStore(storeModel)));
                response = storeActionResult;
            }
            catch (Exception e) {
                Log.Error($"Failed to add store due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }
        [NonAction]
        public IActionResult AddStore(StoreModel storeModel)
        {
            IActionResult response = null;
            StoreResponseModel ResponseModel = new StoreResponseModel();
            StoreModel addedStore = null;

            addedStore = new StoreBL(storeRepository).AddStore(storeModel).Result;

            if (addedStore != null)
            {
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0401", Text = "Store added successfully" } };
                ResponseModel.ResultCode = "OK";
                ResponseModel.Messages = messages;
                ResponseModel.Id = storeModel.StoreId;
                response = Created($"/api/store/get/{addedStore.StoreId}", ResponseModel);
            }
            else
            {
                DisplayMessage("Failed to process the request");
            }
            return response;
        }
        [NonAction]
        public IActionResult UpdateStore(StoreModel storeModel)
        {
            IActionResult response = null;
            StoreResponseModel ResponseModel = new StoreResponseModel();
            bool StoreAdded = new StoreBL(storeRepository).UpdateStore(storeModel).Result;
            if (StoreAdded)
            {
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0401", Text = "Store updated successfully" } };
                ResponseModel.Id = storeModel.StoreId;
                ResponseModel.ResultCode = "OK";
                ResponseModel.Messages = messages;
            }
            else
            {
                DisplayMessage("Failed to process the request");
            }
            response = Ok(ResponseModel);
            return response;
        }
        [NonAction]
        public IActionResult DisplayMessage(string Message)
        {
            IActionResult response = null;
            StoreResponseModel ResponseModel = new StoreResponseModel();
            List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() {
                        new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = Message } };
            ResponseModel.ResultCode = "ERR";
            ResponseModel.Messages = messages;
            response = Ok(ResponseModel);
            return response;
        }
        /// <summary>
        /// Update an existing store
        /// </summary>
        /// <returns></returns>
        [HttpPost("update")]
        public IActionResult ProcessUpdateStore(StoreRequestModel storeRequestModel)
        {

            IActionResult response = null;
            StoreResponseModel ResponseModel = new StoreResponseModel();
            try
            {
                StoreModel IsExists = null;
                StoreModel IsStoreNameExists = null;
                StoreModel IsStoreClientIdExists = null;
                storeBL = new StoreBL(storeRepository);
                IActionResult storeActionResult = null;
                try
                {
                    IsExists = storeBL.GetStore(storeRequestModel.StoreId);
                    if (IsExists.Name != storeRequestModel.Name)
                    {
                        IsStoreNameExists = storeBL.GetStore(storeRequestModel.OrganizationId, storeRequestModel.Name);
                    }
                    if (IsExists.StoreClientId != storeRequestModel.StoreClientId)
                    {
                        IsStoreClientIdExists = storeBL.GetStoreClientId(storeRequestModel.OrganizationId, storeRequestModel.StoreClientId);
                    }
                }
                catch (Exception e) {
                    Log.Error(e.Message);
                }
                
                if (IsExists != null) 
                {
                            StoreModel storeModel = new StoreModel()
                            {
                                StoreId = IsExists.StoreId,
                                OrganizationId = IsExists.OrganizationId,
                                Id = IsExists.Id,
                                Name = storeRequestModel.Name,
                                StoreClientId = storeRequestModel.StoreClientId,
                                TimeZone = storeRequestModel.TimeZone,
                                StoreOpenTime = storeRequestModel.StoreOpenTime,
                                StoreCloseTime = storeRequestModel.StoreCloseTime,
                                AllowedPeople = storeRequestModel.AllowedPeople,
                                GeoLocation = storeRequestModel.GeoLocation,
                                BussinessContact = storeRequestModel.BussinessContact,
                                TechnicalContact = storeRequestModel.TechnicalContact
                            };
                    string StoreNameExists = "Store name exists";
                    string StoreClientIdExistsMessage = "Store Client Id exists";
                 

                    storeActionResult = (IsStoreNameExists != null) ? DisplayMessage(StoreNameExists) :
                              (IsStoreClientIdExists != null) ? DisplayMessage(StoreClientIdExistsMessage) : UpdateStore(storeModel);
                   
                }
                else {
                    storeActionResult = DisplayMessage("Store doesn't exists");
                }
                response = storeActionResult;
            }
            catch (Exception e)
            {
                Log.Error($"Failed to update the store due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }
        /// <summary>
        /// Activate a store
        /// </summary>
        /// <returns></returns>
        [HttpPost("activate/{storeId}")]
        public IActionResult ActivateStore(int storeId)
        {
            return this.ToggleActivation(storeId,true);
            
        }
        /// <summary>
        /// Deactivate a store
        /// </summary>
        /// <returns></returns>

        [HttpPost("deactivate/{storeId}")]
        public IActionResult DeactivateStore(int storeId)
        {
            return this.ToggleActivation(storeId, false);

        }
        [NonAction]
        public IActionResult ToggleActivation(int storeId, bool flag) {
            IActionResult response = null;
            StoreResponseModel ResponseModel = new StoreResponseModel();
            try
            {
                storeBL = new StoreBL(storeRepository);
                StoreModel ExistingStore = storeBL.GetStore(storeId);
                if (ExistingStore != null)
                {
                    ExistingStore.IsActive = flag;
                    bool StoreAdded = new StoreBL(storeRepository).UpdateStore(ExistingStore).Result;
                    if (StoreAdded)
                    {
                        List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0401", Text = "Completed successfully" } };
                        ResponseModel.ResultCode = "OK";
                        ResponseModel.Id = ExistingStore.StoreId;
                        ResponseModel.Messages = messages;
                    }
                    else
                    {
                        DisplayMessage("Failed to process the request");
                    }

                    response = Ok(ResponseModel);
                }
                else
                {
                    DisplayMessage("Store doesn't exists");
                }

            }
            catch (Exception e)
            {
                Log.Error($"Failed to update the store due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }


    }
}