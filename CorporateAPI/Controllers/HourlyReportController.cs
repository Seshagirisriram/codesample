﻿namespace CorporateAPI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CorporateAPI.BL;
    using CorporateAPI.Dto;
    using CorporateAPI.Models.Request;
    using CorporateAPI.Models.Response;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using MongoDB.Bson;
    using YamlDotNet.Serialization.ObjectFactories;
    using log4net;
    [Route("api/report/hourly")]
    [ApiController]
    public class HourlyReportController : ControllerBase
    {
        /// <summary>
        /// Static Logger
        /// </summary>
        public static ILog Log = LogManager.GetLogger(typeof(HourlyReportController));
        [HttpPost]
        public IActionResult GetReports([FromBody] HourlyReportRequestModel request)
        {
            try
            {
                HourlyReportDto response = new HourlyReportDto();
                if (this.ModelState.IsValid)
                {
                    return Ok(new HourlyReportBL().GetReports(request));
                }
                else
                {
                    return BadRequest(ModelState["error"]);
                }
            }
            catch (Exception e)
            {
                Log.Error("GetReport(): " + e.Message);
                return BadRequest(ModelState["error"]);
            }
        }
    }
}
