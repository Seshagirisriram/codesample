﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorporateAPI.BL;
using CorporateAPI.Models.Request;
using CorporateAPI.Models.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using log4net;
using CorporateAPI.Exceptions;
using Microsoft.AspNetCore.Authorization;
using CorporateAPI.Models;
using CorporateAPI.DatabaseService;

namespace CorporateAPI.Controllers
{
    [Authorize]
    [Route("api/store/[controller]")]
    [ApiController]
    public class DeviceController : ControllerBase
    {

        /// <summary>
        /// Static Logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(DeviceController));
       
        private IMongoDBService<StoreModel> storeRepository { get; set; }
        private IMongoDBService<DeviceModel> deviceRepository { get; set; }
        public IDeviceBL deviceBL { get; set; }

        /// <summary>
        /// Find device by Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("get/{deviceId}")]
        public IActionResult GetDevice(int deviceId) {
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            try
            {
                deviceBL = new DeviceBL(storeRepository,deviceRepository);
                DeviceModel deviceModel = deviceBL.GetDevice(deviceId).Result;
                if (deviceModel == null)
                {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0502", Text = "Device doesn't exist" } };
                    responseModel.ResultCode = "ERR";
                    responseModel.Messages = messages;
                    response = Ok(responseModel);
                }
                else { response = Ok(deviceModel);}
                
            }
            catch (Exception e)
            {
                Log.Error($"Failed to get the device details due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0502", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            return response;
        }
        /// <summary>
        /// Get all the devices in a store
        /// </summary>
        /// <returns></returns>
        [HttpGet("getAll/{storeId}")]
        public IActionResult GetDevices(int storeId)
        {
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            deviceBL = new DeviceBL(storeRepository,deviceRepository);
            try
            {
                string DeviceNotExistsMessage = "Device doesn't exists for the given Store Id";
                List<DeviceModel> deviceModel = deviceBL.GetDevices(storeId);
                response = (deviceModel != null)?Ok(deviceModel):DisplayMessage(DeviceNotExistsMessage);
            }
            catch (Exception e)
            {
                Log.Error($"Failed to get the device details due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0502", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            return response;
        }
        /// <summary>
        /// Add a device to a store
        /// </summary>
        /// <returns></returns>
        [HttpPost("add")]
        public IActionResult ProcessAddDeviceRequest(DeviceRequestModel deviceRequestModel)
        {

            IActionResult response = null;
            DeviceResponseModel ResponseModel = new DeviceResponseModel();
            try
            {
                deviceBL = new DeviceBL(storeRepository, deviceRepository);
                StoreBL storeBL = new StoreBL(storeRepository);
                DeviceModel AlreadyExistingDevice = deviceBL.GetDeviceBySerialNumber(deviceRequestModel.SerialNumber);
                StoreModel IsStoreIdExists = storeBL.GetStore(deviceRequestModel.StoreId);
                DeviceModel IsSerialNumberExists = deviceBL.GetTypeBySerialNumber(deviceRequestModel.SerialNumber,deviceRequestModel.Type);

                string SerialNumberExistsMessage = "Device with the same serial number already exists";
                string StoreIdNotExistsMessage = "Store Id doesn't exist";
                string TypeExistsMessage = "Type with the same serial number already exists";


                IActionResult deviceActionResult = (AlreadyExistingDevice != null) ? DisplayMessage(SerialNumberExistsMessage) :
                           (IsStoreIdExists == null) ? DisplayMessage(StoreIdNotExistsMessage) :
                           (IsSerialNumberExists != null) ? DisplayMessage(TypeExistsMessage) : AddDevice(deviceRequestModel);
                response = deviceActionResult;
            }
            catch (StoreNotFoundException e) {
                Log.Error($"Failed to add device due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0405", Text = e.Message } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
              }
            catch (Exception e)
            {
                Log.Error($"Failed to add device due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0405", Text = e.InnerException.Message } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }
        [NonAction]
        public IActionResult DisplayMessage(string Message)
        {
            IActionResult response = null;
            DeviceResponseModel ResponseModel = new DeviceResponseModel();
            List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() {
                        new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0412", Text = Message } };
            ResponseModel.ResultCode = "ERR";
            ResponseModel.Messages = messages;
            response = Ok(ResponseModel);
            return response;
        }
        [NonAction]
        public IActionResult AddDevice(DeviceRequestModel deviceRequestModel)
        {
            IActionResult response = null;
            DeviceResponseModel ResponseModel = new DeviceResponseModel();
            DeviceModel addedDevice = deviceBL.AddDevice(deviceRequestModel).Result;
            if (addedDevice != null)
            {
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>()
                    { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0404", Text = "Device added successfully" } };
                ResponseModel.ResultCode = "OK";
                ResponseModel.Messages = messages;
                ResponseModel.Id = addedDevice.DeviceId;
                response = Created($"/api/store/device/get/{addedDevice.DeviceId}", ResponseModel);
            }
            else
            {
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() {
                        new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0406", Text = "Failed to process the request" } };
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = Ok(ResponseModel);
            }
            return response;
        }
        /// <summary>
        /// Update an existing device
        /// </summary>
        /// <returns></returns>
        [HttpPost("update")]
        public IActionResult ProcessUpdateDeviceRequest(DeviceRequestModel deviceRequestModel)
        {

            IActionResult response = null;
            DeviceResponseModel ResponseModel = new DeviceResponseModel();
            try
            {
                deviceBL = new DeviceBL(storeRepository, deviceRepository);
                StoreBL storeBL = new StoreBL(storeRepository);
                DeviceModel deviceModel = deviceBL.GetDevice(deviceRequestModel.DeviceId).Result;
                DeviceModel AlreadyExistingDevice = null;
                StoreModel IsStoreIdExists = null;
                DeviceModel IsSerialNumberExists = null;
                IActionResult deviceActionResult = null;
                if (deviceModel != null)
                {
                    if (deviceModel.SerialNumber != deviceRequestModel.SerialNumber)
                        AlreadyExistingDevice = deviceBL.GetDeviceBySerialNumber(deviceRequestModel.SerialNumber);
                    if (deviceModel.StoreId != deviceRequestModel.StoreId)
                        IsStoreIdExists = storeBL.GetStore(deviceRequestModel.StoreId);
                    if ((deviceModel.SerialNumber != deviceRequestModel.SerialNumber) && (deviceModel.Type != deviceRequestModel.Type))
                        IsSerialNumberExists = deviceBL.GetTypeBySerialNumber(deviceRequestModel.SerialNumber, deviceRequestModel.Type);

                    string SerialNumberExistsMessage = "Device with the same serial number already exists";
                    string StoreIdNotExistsMessage = "Store Id doesn't exists";
                    string TypeExistsMessage = "Type with the same serial number already exists";


                    deviceActionResult = (AlreadyExistingDevice != null) ? DisplayMessage(SerialNumberExistsMessage) :
                               (IsStoreIdExists == null) ? DisplayMessage(StoreIdNotExistsMessage) :
                               (IsSerialNumberExists != null) ? DisplayMessage(TypeExistsMessage) : UpdateDevice(deviceRequestModel);
                }
                else
                {
                    deviceActionResult = DisplayMessage("Device doesn't exist");
                }
                response = deviceActionResult;
            
            }
            catch (DeviceNotFoundException e)
            {
                Log.Error($"Failed to update device due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0405", Text = e.Message } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            catch (Exception e)
            {
                Log.Error($"Failed to update device due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0405", Text = e.InnerException.Message } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }
        [NonAction]
        public IActionResult UpdateDevice(DeviceRequestModel deviceRequestModel)
        {
            IActionResult response = null;
            string DeviceNotExistsMessage = "Device doesn't exist";
            DeviceResponseModel ResponseModel = new DeviceResponseModel();
            deviceBL = new DeviceBL(storeRepository, deviceRepository);
            DeviceModel deviceModel = deviceBL.GetDevice(deviceRequestModel.DeviceId).Result;

            if (deviceModel != null)
            {
                deviceModel.IpAddress = deviceRequestModel.IpAddress;
                deviceModel.MacAddress = deviceRequestModel.MacAddress;
                deviceModel.SerialNumber = deviceRequestModel.SerialNumber;
                deviceModel.Type = deviceRequestModel.Type;
                bool deviceAdded = deviceBL.UpdateDevice(deviceModel).Result;
                if (deviceAdded)
                {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>()
                { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0404", Text = "Device updated successfully" } };
                    ResponseModel.ResultCode = "OK";
                    ResponseModel.Messages = messages;
                }
                else
                {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() {
                    new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0406", Text = "Failed to process the request" } };
                    ResponseModel.ResultCode = "ERR";
                    ResponseModel.Messages = messages;
                }
                response = Ok(ResponseModel);
            }
            else { response = DisplayMessage(DeviceNotExistsMessage); }
            return response;
        }
        /// <summary>
        /// Activate a device
        /// </summary>
        /// <returns></returns>
        [HttpPost("activate/{deviceId}")]
        public IActionResult ActivateDevice(int deviceId)
        {
            return this.ToggleActivation(deviceId, true);

        }
        /// <summary>
        /// Deactivate an existing device
        /// </summary>
        /// <returns></returns>
        [HttpPost("deactivate/{deviceId}")]
        public IActionResult DeactivateDevice(int deviceId)
        {
            return this.ToggleActivation(deviceId, false);

        }
        [NonAction]
        public IActionResult ToggleActivation(int deviceId, bool flag)
        {
            IActionResult response = null; 
            DeviceResponseModel ResponseModel = new DeviceResponseModel();
            try
            {
                deviceBL = new DeviceBL(storeRepository, deviceRepository);
                DeviceModel ExistingDevice = deviceBL.GetDevice(deviceId).Result;
                if (ExistingDevice != null)
                {
                    ExistingDevice.IsActive = flag;
                    bool DeviceUpdated = deviceBL.UpdateDevice(ExistingDevice).Result;

                    // Consider rewriting like so
                    /// List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0401", Text = "Completed successfully" } };
                    // ResponseModel.ResultCode = "OK";
                    // ResponseModel.Id = ExistingDevice.DeviceId;
                    // ResponseModel.Messages = messages;
                    // if (!DeviceUpdated) ResponseModel.ResultCode = "ERR";
                    if (DeviceUpdated)
                    {
                        List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0401", Text = "Completed successfully" } };
                        ResponseModel.ResultCode = "OK";
                        ResponseModel.Id = ExistingDevice.DeviceId;
                        ResponseModel.Messages = messages;
                    }
                    else
                    {
                        List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                        ResponseModel.ResultCode = "ERR";
                        ResponseModel.Messages = messages;
                    }

                    response = Ok(ResponseModel);
                }
                else
                {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Device doen't exitsts" } };
                    ResponseModel.ResultCode = "ERR";
                    ResponseModel.Messages = messages;
                    response = Ok(ResponseModel);
                }

            }
            catch (Exception e)
            {
                Log.Error($"Failed to update device due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }
    }
}