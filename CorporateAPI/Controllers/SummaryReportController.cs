﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorporateAPI.BL;
using CorporateAPI.Dto;
using CorporateAPI.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using log4net; 

namespace CorporateAPI.Controllers
{
    [Route("api/report/summary")]
    [ApiController]
    public class SummaryReportController : ControllerBase
    {
        /// <summary>
        /// Static Logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(SummaryReportController));
        [HttpPost]
        public List<SummaryReportDto> GetReports(SummaryReportRequestModel request)
        {
            // See comments in BL about proper handling of exception thrown in middle of loop. 
            try
            {
                return new SummaryReportBL().GetReport(request);
            } 
            catch (Exception e)
            {
                Log.Error(e.Message); // SRIRAM: NOTE: I DO NOT USE e.StackTrace. This is pretty much uselesss. 
                // unless we have a better mode, this will do. 
                return new List<SummaryReportDto>(); // return empty list.... 
            }

        }
    }

}