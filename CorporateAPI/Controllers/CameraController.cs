﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorporateAPI.BL;
using CorporateAPI.Helpers;
using CorporateAPI.Models;
using CorporateAPI.Models.Request;
using CorporateAPI.Models.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using log4net;
using CorporateAPI.ModelBinders;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.FileProviders;
using CorporateAPI.DatabaseService;
using CorporateAPI.Exceptions;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using ImageResizingDrawing;

namespace CorporateAPI.Controllers
{
    /// <summary>
    /// Camera Controller 
    /// </summary>
    [Authorize]
    [Route("api/store/[controller]")]
    [ApiController]
    public class CameraController : ControllerBase
    {

        /// <summary>
        /// Static Logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(CameraController));
        public ICameraBL cameraBL { get; set; }
        public IZonesBL zoneBL { get; set; }
        private IMongoDBService<StoreModel> storeRepository { get; set; }
        private IMongoDBService<CameraModel> cameraRepository { get; set; }
        private IMongoDBService<ZoneModel> zoneRepository { get; set; }
       
        /// <summary>
        /// Find camera by Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("get/{cameraId}")]
        public IActionResult GetCamera(int cameraId) {
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            try
            {
                cameraBL = new CameraBL(cameraRepository,storeRepository);
                CameraModel cameraModel = cameraBL.GetCamera(cameraId);
                if (cameraModel != null)
                {
                    response = Ok(cameraModel);
                }
                else {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0413", Text = "Camera doesn't exists" } };
                    responseModel.ResultCode = "ERR";
                    responseModel.Messages = messages;
                    response = Ok(responseModel);
                }
            }
            catch (Exception e)
            {
                Log.Error($"Failed to get the camera details due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0408", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            return response;
        }

        /// <summary>
        /// Get all the cameras in a store 
        /// </summary>
        /// <returns></returns>
        [HttpGet("getAll/{storeId}")]
        public IActionResult GetCameras(int storeId)
        {
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            cameraBL = new CameraBL(cameraRepository, storeRepository);
            try
            {
                List<CameraModel> cameraModel = cameraBL.GetCameras(storeId);
                response = Ok(cameraModel);
            }
            catch (Exception e)
            {
                Log.Error($"Failed to get the camera details due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0502", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            return response;
        }
        /// <summary>
        /// Add a new camera to store
        /// </summary>
        /// <returns></returns>
        [HttpPost("add")]
        public IActionResult ProcessAddCamera(CameraRequestModel cameraRequestModel) {
            IActionResult response = null;
            CameraResponseModel ResponseModel = new CameraResponseModel();
            CameraModel IsIpAddressExists = null;
            cameraBL = new CameraBL(cameraRepository, storeRepository);
            StoreBL storeBL = new StoreBL(storeRepository);
        
            try
            {
                StoreModel storeModel = storeBL.GetStore(cameraRequestModel.StoreClientId);
                if(storeModel!=null)
                 IsIpAddressExists = cameraBL.GetIpAddressByStoreId(cameraRequestModel.CameraIp, storeModel.StoreId);

                string ipAddressExistsMessage="IP Address with the same store client id already exists";
                string storeClientIdNotExistsMessage = "Store Client Id doesn't exists";

                IActionResult cameraActionResult = (storeModel == null) ? DisplayMessage(storeClientIdNotExistsMessage) :
                              (IsIpAddressExists != null) ? DisplayMessage(ipAddressExistsMessage) : AddCamera(cameraRequestModel);
                response = cameraActionResult; 
            }
            catch (Exception e)
            {
                Log.Error($"Failed to add camera due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0408", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }
        [NonAction]
        public IActionResult DisplayMessage(string Message)
        {
            IActionResult response = null;
            CameraResponseModel ResponseModel = new CameraResponseModel();
            List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() {
                        new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0412", Text = Message } };
            ResponseModel.ResultCode = "ERR";
            ResponseModel.Messages = messages;
            response = Ok(ResponseModel);
            return response;
        }
        [NonAction]
        public IActionResult AddCamera(CameraRequestModel cameraRequestModel)
        {
            IActionResult response = null;
            CameraResponseModel ResponseModel = new CameraResponseModel();
            CameraModel cameraModel = cameraBL.AddCamera(cameraRequestModel).Result;
            if (cameraModel != null)
            {
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0407", Text = "Camera added successfully" } };
                ResponseModel.ResultCode = "OK";
                ResponseModel.CameraId = cameraModel.CameraId;
                ResponseModel.Messages = messages;
                response = Created($"/api/store/camera/get/{cameraModel.CameraId}", ResponseModel);
            }
            else
            {
                response = DisplayMessage("Failed to process the request");
                Log.Error("Failed to add Camera in AddCamera(): " + cameraRequestModel.CameraId); 
            }
            return response;
        }
        /// <summary>
        /// Update an existing camera
        /// </summary>
        /// <returns></returns>
        [HttpPost("update")]
        public IActionResult ProcessUpdateCamera(CameraRequestModel cameraRequestModel)
        {
            IActionResult response = null;
            IActionResult cameraActionResult = null;
            CameraResponseModel ResponseModel = new CameraResponseModel();
            try
            {
                CameraModel IsExists = null;
                cameraBL = new CameraBL(cameraRepository, storeRepository);
                StoreBL storeBL = new StoreBL(storeRepository);
                try
                {
                    StoreModel storeModel = storeBL.GetStore(cameraRequestModel.StoreClientId);
                    IsExists = cameraBL.GetCamera(cameraRequestModel.CameraId);
                }
                catch (Exception e)
                {
                    Log.Error(e.Message);
                }

                if (IsExists != null)
                {
                        CameraModel IsIpAddressExists = null;
                        CameraModel cameraModel = new CameraModel()
                        {
                            Id = IsExists.Id,
                            CameraId = cameraRequestModel.CameraId,
                            StoreId = IsExists.StoreId,
                            CameraIp = cameraRequestModel.CameraIp,
                            CameraName = cameraRequestModel.CameraName,
                            UserName = cameraRequestModel.UserName,
                            Password = cameraRequestModel.Password,
                            Murl = cameraRequestModel.Murl,
                            MurlSuffix = cameraRequestModel.MurlSuffix,
                            Protocol = cameraRequestModel.Protocol,
                            StoreClientId=IsExists.StoreClientId             
                        };

                        if ((IsExists.CameraIp != cameraRequestModel.CameraIp)||(cameraRequestModel.StoreClientId != IsExists.StoreClientId))
                              IsIpAddressExists = cameraBL.GetIpAddressByStoreId(cameraRequestModel.CameraIp, IsExists.StoreId);

                        string ipAddressExistsMessage = "IP Address with the same store client id already exists";
                        cameraActionResult = (IsIpAddressExists != null) ? DisplayMessage(ipAddressExistsMessage) : UpdateCamera(cameraModel);
                        
                }
                else
                {
                    cameraActionResult = DisplayMessage("Camera doesn't exist");
                    Log.Error("Failed to update Camera in ProcessUpdateCamera: " + cameraRequestModel.CameraId);
                }
                response = cameraActionResult;
            }
            catch (Exception e)
            {
                Log.Error($"Failed to update the camera due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = Ok(ResponseModel);
            }
            return response;
        }
        [NonAction]
        public IActionResult UpdateCamera(CameraModel cameraModel)
        {
            IActionResult response = null;
            CameraResponseModel ResponseModel = new CameraResponseModel();
            bool CameraUpdated = new CameraBL(cameraRepository, storeRepository).UpdateCamera(cameraModel).Result;
            if (CameraUpdated)
            {
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0401", Text = "Camera updated successfully" } };
                ResponseModel.CameraId = cameraModel.CameraId;
                ResponseModel.ResultCode = "OK";
                ResponseModel.Messages = messages;
            }
            else
            {
                response = DisplayMessage("Failed to process the request");
                Log.Error("Failed to update Camera in UpdateCamera: " + cameraModel.CameraId);
            }
            response = Ok(ResponseModel);
            return response;
        }
        /// <summary>
        /// Activate a camera
        /// </summary>
        /// <returns></returns>
        [HttpPost("activate/{cameraId}")]
        public IActionResult ActivateCamera(int cameraId)
        {
            return this.ToggleActivation(cameraId, true);

        }
        /// <summary>
        /// Deactivate an existing camera
        /// </summary>
        /// <returns></returns>
        [HttpPost("deactivate/{cameraId}")] 
        public IActionResult DeactivateCamera(int cameraId)
        {
            return this.ToggleActivation(cameraId, false);

        }
        [NonAction]
        public IActionResult ToggleActivation(int cameraId, bool flag)
        {
            IActionResult response = null;
            CameraResponseModel ResponseModel = new CameraResponseModel();
            try
            {
                cameraBL = new CameraBL(cameraRepository, storeRepository);

                CameraModel ExistingCamera = cameraBL.GetCamera(cameraId);
                if (ExistingCamera != null)
                {
                    ExistingCamera.IsActive = flag;
                    bool CameraUpdated = cameraBL.UpdateCamera(ExistingCamera).Result;
                    if (CameraUpdated)
                    {
                        List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0401", Text = "Completed successfully" } };
                        ResponseModel.ResultCode = "OK";
                        ResponseModel.CameraId = ExistingCamera.CameraId;
                        ResponseModel.Messages = messages;
                    }
                    else
                    {
                        DisplayMessage("Failed to process the request");
                        Log.Error("Failed to toggle camera activation for camera ID:" + cameraId + " with value of: " +flag);
                    }

                    response = Ok(ResponseModel);
                }
                else
                {
                    response = DisplayMessage("Camera doesn't exist");
                    Log.Error("Failed to toggle camera activation for camera ID:" + cameraId + " with value of: " + flag + " because camera does not exist."); 
                }

            }
            catch (Exception e)
            {
                Log.Error($"Failed to update the camera due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }
        /// <summary>
        /// Download zone file by Id
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("zone/download/{zoneId}")]
        public IActionResult DownloadZoneFile(int zoneId, string filePath = null)
        {
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            try
            {
                string uploadPath = System.Configuration.ConfigurationManager.AppSettings["uploadPath"];
                String DirectoryPath = (uploadPath != null) ? uploadPath : AppSettings.uploadPath;

                //check if the path exist
                IFileProvider provider = new PhysicalFileProvider(DirectoryPath);
                String fileName = $"{zoneId}.yml";
                IFileInfo fileInfo = provider.GetFileInfo(fileName);
                var readStream = fileInfo.CreateReadStream();
                var mimeType = "application/octet-stream";
                return File(readStream, mimeType, "download.yml");
             
            }
            catch (Exception e) {
                Log.Error($"Failed to download the file, due to the following error: {e.Message}");
                Log.Error(e.StackTrace);

                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0408", Text = "File Not Found" } };
                //Response.StatusCode = 404;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
           
            return response;
        }
        /// <summary>
        /// Find a zone by Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("zone/get/{zoneId}")]
        public IActionResult GetZone(int zoneId) {
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            zoneBL = new ZonesBL(zoneRepository);
            try
            {
                ZoneModel zoneModel = zoneBL.GetZone(zoneId);
                if (zoneModel != null)
                {
                    response = Ok(zoneModel);
                }
                else {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0414", Text = "Zone doesn't exists" } };
                    responseModel.ResultCode = "ERR";
                    responseModel.Messages = messages;
                    response = Ok(responseModel);
                }
                
            }
            catch (Exception e)
            {
                Log.Error($"Failed to get the zone details due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0408", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            return response;
        }
        /// <summary>
        /// Get all zones by Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("zone/getAll/{cameraId}")]
        public IActionResult GetZones(int cameraId)
        {
            IActionResult response = null;
            BaseResponseModel responseModel = new BaseResponseModel();
            zoneBL = new ZonesBL(zoneRepository);
            try
            {
                 List<ZoneModel> zoneModel = zoneBL.GetZones(cameraId);
                 response = Ok(zoneModel);
            }
            catch (Exception e)
            {
                Log.Error($"Failed to get the zone details due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0408", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                responseModel.ResultCode = "ERR";
                responseModel.Messages = messages;
                response = BadRequest(responseModel);
            }
            return response;
        }
        
        /// <summary>
        /// Add a zone 
        /// </summary>
        /// <returns></returns>

        [HttpPost("zone/add")]
        public IActionResult ProcessAddZone([ModelBinder(BinderType = typeof(JsonModelBinder))] ZoneRequestModel zoneRequestModel, IFormFile file)
        {
            IActionResult response = null;
            BaseResponseModel ResponseModel = new BaseResponseModel();
            IActionResult cameraActionResult = null;
            cameraBL = new CameraBL(cameraRepository, storeRepository);
            zoneBL = new ZonesBL(zoneRepository);
            try
            {
                if (file != null)
                {
                    String FileExtension = Path.GetExtension(file?.FileName);
                    if (FileExtension != ".json")
                    {
                        List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message()
                    { Code = "ARCA-E0410", Text = "The zone file uploaded not maching the desired format, the expected format is *.json" } };
                        ResponseModel.ResultCode = "ERR";
                        ResponseModel.Messages = messages;
                        response = BadRequest(ResponseModel);
                    }
                    else
                    {
                        CameraModel IsCameraIdExists = cameraBL.GetCamera(zoneRequestModel.CameraId);
                        string uploadPath = System.Configuration.ConfigurationManager.AppSettings["uploadPath"];
                        string DirectoryPath = (uploadPath != null) ? uploadPath : AppSettings.uploadPath;

                        string jsonFilePath = Path.Combine(DirectoryPath, file?.FileName);
                

                        using (var stream = new FileStream(jsonFilePath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }
                        //JSON file
                        string json = System.IO.File.ReadAllText(jsonFilePath);

                        //JSON Deserialization
                        ZoneListModel jsonResponse = new ZoneListModel();
                        jsonResponse = JsonConvert.DeserializeObject<ZoneListModel>(json);
                        JsonValidations validateZone = new JsonValidations();

                        if (jsonResponse != null)
                        {
                            validateZone.ValidateJson(jsonResponse, zoneRequestModel.CameraId);
                        }

                        CameraModel IsCameraIpExists = cameraBL.GetIpAddressByCameraId(jsonResponse.camera_ip, zoneRequestModel.CameraId);

                        cameraActionResult = (IsCameraIdExists == null) ? DisplayMessage("Camera Id doesn't exists") :
                            (IsCameraIpExists == null) ? DisplayMessage("Camera IP doesn't exists") : AddZone(zoneRequestModel, jsonFilePath);
                       
                    }
                }
                else
                {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0411", Text = "File is empty" } };
                    ResponseModel.ResultCode = "ERR";
                    ResponseModel.Messages = messages;
                    response = BadRequest(ResponseModel);
                }
                response = cameraActionResult;
                   
            }
            catch (Exception e)
            {
                Log.Error($"Failed to add zone due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0411", Text = $"Failed to add zone due to the following error: {e.Message}" } };
                // Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }
        [NonAction]
        public IActionResult AddZone([ModelBinder(BinderType = typeof(JsonModelBinder))] ZoneRequestModel zoneRequestModel, string jsonFile)
        {
            IActionResult response = null;
            BaseResponseModel ResponseModel = new BaseResponseModel();
            zoneBL = new ZonesBL(zoneRepository);

            try
            {
              ZoneModel zoneModel = zoneBL.AddZone(zoneRequestModel).Result;
              if (zoneModel != null)
              {
                var sourceFilePath = cameraBL.GetCamera(zoneModel.CameraId);
                ZoneResizingDrawing drawing = new ZoneResizingDrawing();//"D:\\Cogniphi\\ymlUpload\\OriginalImage.jpg";//
                drawing.DrawZones(jsonFile, sourceFilePath.CameraSourceFilePath);
                //drawing.DrawZones(jsonFile, "D:\\Cogniphi\\ymlUpload\\OriginalImage.jpg");

                List <CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0409", Text = "Zone added successfully" } };
                ResponseModel.ResultCode = "OK";
                ResponseModel.Messages = messages;
                ResponseModel.Id = zoneModel.ZoneId;
                response = Created($"/api/store/camera/zone/get/{zoneModel.ZoneId}", ResponseModel);
               }
               else
               {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARFA-E0203", Text = "Failed to add zone" } };
                    ResponseModel.ResultCode = "ERR";
                    ResponseModel.Messages = messages;
                    response = Ok(ResponseModel);
               }
            }
            catch (Exception e)
            {

                Log.Error($"Failed to add zone due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0411", Text = "Failed to process the request- "+e.Message } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }

        /// <summary>
        /// Update an existing zone
        /// </summary>
        /// <returns></returns>
        [HttpPost("zone/update")]
        public IActionResult ProcessUpdateZone([ModelBinder(BinderType = typeof(JsonModelBinder))] ZoneRequestModel zoneRequestModel, IFormFile file)
        {
            IActionResult response = null;
            IActionResult cameraActionResult = null;
            BaseResponseModel ResponseModel = new BaseResponseModel();
            zoneBL = new ZonesBL(zoneRepository);
            cameraBL = new CameraBL(cameraRepository, storeRepository);
            try
            {
                ZoneModel IsExists = null;
                try
                {
                    IsExists = zoneBL.GetZone(zoneRequestModel.SystemZoneId);
                }
                catch (Exception e)
                {
                    Log.Error(e.Message);
                }

                if (IsExists != null)
                {
                    ZoneModel zoneModel = new ZoneModel()
                    {
                        ZoneId=IsExists.ZoneId,
                        Id = IsExists.Id,
                        CameraId = zoneRequestModel.CameraId,
                        StoreId = IsExists.StoreId,
                        SystemZoneId= IsExists.SystemZoneId,
                        PremiseZoneId= IsExists.PremiseZoneId,
                        SystemZoneName=zoneRequestModel.SystemZoneName,
                        PremiseZoneName=zoneRequestModel.PremiseZoneName,
                        SceneCoordinates=zoneRequestModel.SceneCoordinates,
                        LayoutCoordinates=zoneRequestModel.LayoutCoordinates,
                        InZoneCount=zoneRequestModel.InZoneCount,
                        ZoneClientId=zoneRequestModel.ZoneClientId
                    };

                     CameraModel IsCameraIdExists = cameraBL.GetCamera(zoneRequestModel.CameraId);
                     cameraActionResult = (IsCameraIdExists == null) ? DisplayMessage("Camera Id doesn't exists") : UpdateZone(zoneModel, file);

                }
                else
                {
                        cameraActionResult=DisplayMessage("Zone doesn't exist");
                    Log.Error("Failed to process Zone in ProcessZoneUpdate(): cameraid: " + zoneRequestModel.CameraId + ", System zoneid: " + zoneRequestModel.SystemZoneId + ", Premise Zone ID" + zoneRequestModel.PremiseZoneId); 
                }
                response = cameraActionResult;
            }
            catch (Exception e)
            {
                Log.Error($"Failed to update the zone due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }
        [NonAction]
        public IActionResult UpdateZone([ModelBinder(BinderType = typeof(JsonModelBinder))] ZoneModel zoneModel, IFormFile file)
        {
            IActionResult response = null;
            IActionResult cameraActionResult = null;
            BaseResponseModel ResponseModel = new BaseResponseModel();
            zoneBL = new ZonesBL(zoneRepository);
            String DirectoryPath = System.Configuration.ConfigurationManager.AppSettings["uploadPath"];
            try
            {
                if (!Directory.Exists(DirectoryPath))
                {
                    Directory.CreateDirectory(DirectoryPath);
                    // THIS CAN AND WILL THROW EXCEPTION IF FILE PERMIMISSIONS ARE NOT AVAILABLE... 
                }

                bool ZoneUpdated = zoneBL.UpdateZone(zoneModel).Result;
               
                if (ZoneUpdated)
                {
                    if (file != null)
                    {
                        String FileExtension = Path.GetExtension(file?.FileName);
                        if (FileExtension != ".json")
                        {
                            List<CorporateAPI.Models.Response.Message> message = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message()
                        { Code = "ARCA-E0410", Text = "The zone file uploaded not maching the desired format, the expected format is *.yml" } };
                            ResponseModel.ResultCode = "ERR";
                            ResponseModel.Messages = message;
                            response = BadRequest(ResponseModel);
                        }
                        else
                        {
                            

                            string jsonFilePath = Path.Combine(DirectoryPath, file?.FileName);

                            //JSON file
                            string json = System.IO.File.ReadAllText(jsonFilePath);

                            //JSON Deserialization
                            ZoneListModel jsonResponse = new ZoneListModel();
                            jsonResponse = JsonConvert.DeserializeObject<ZoneListModel>(json);
                            JsonValidations validateZone = new JsonValidations();

                            if (jsonResponse != null)
                            {
                                validateZone.ValidateJson(jsonResponse, zoneModel.CameraId);
                            }

                            CameraModel IsCameraIpExists = cameraBL.GetIpAddressByCameraId(jsonResponse.camera_ip, zoneModel.CameraId);
                            String finalName = Path.Combine(DirectoryPath, $"{zoneModel.ZoneId}{FileExtension}");
                            cameraActionResult = (IsCameraIpExists == null) ? DisplayMessage("Camera IP doesn't exists") : UploadFile(finalName, zoneModel.CameraId);
                       
                        }
                    }
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0409", Text = "Zone updated successfully" } };
                    ResponseModel.ResultCode = "OK";
                    ResponseModel.Messages = messages;
                    ResponseModel.Id = zoneModel.ZoneId;
                    response = Created($"/api/store/camera/zone/get/{zoneModel.ZoneId}", ResponseModel);

                }
                else
                {
                    List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                    ResponseModel.ResultCode = "ERR";
                    ResponseModel.Messages = messages;

                }
                response = Ok(ResponseModel);
            }
            catch (Exception e)
            {
                // Excessively Large method. 
                // Avoid generic Exceptions.. 
                Log.Error($"Failed to add zone due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0411", Text = "Failed to process the request- " + e.Message } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }
        [NonAction]
        public IActionResult UploadFile(string FilePath,int cameraId)
        {
            // No Try/Catch Blocl????
            IActionResult response = null;
            var sourceFilePath = cameraBL.GetCamera(cameraId);
            ZoneResizingDrawing drawing = new ZoneResizingDrawing();//"D:\\Cogniphi\\ymlUpload\\OriginalImage.jpg";//
            drawing.DrawZones(FilePath, sourceFilePath.CameraSourceFilePath);
           
            return response;
        }
        /// <summary>
        /// Activate a zone
        /// </summary>
        /// <returns></returns>
        [HttpPost("zone/activate/{zoneId}")]
        public IActionResult ActivateZone(int zoneId)
        {
            return this.ToggleZoneActivation(zoneId, true);

        }
        /// <summary>
        /// Deactivate a zone by Id
        /// </summary>
        /// <returns></returns>
        [HttpPost("zone/deactivate/{zoneId}")]
        public IActionResult DeactivateZone(int zoneId)
        {
            return this.ToggleZoneActivation(zoneId, false);

        }
        [NonAction]
        public IActionResult ToggleZoneActivation(int zoneId, bool flag)
        {
            IActionResult response = null;
            BaseResponseModel ResponseModel = new BaseResponseModel();
            zoneBL = new ZonesBL(zoneRepository);
            try
            {
                ZoneModel ExistingZone = zoneBL.GetZone(zoneId);
                if (ExistingZone != null)
                {
                    ExistingZone.IsActive = flag;
                    bool ZoneUpdated = zoneBL.UpdateZone(ExistingZone).Result;
                    if (ZoneUpdated)
                    {
                        List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-S0401", Text = "Completed successfully" } };
                        ResponseModel.ResultCode = "OK";
                        ResponseModel.Id = ExistingZone.ZoneId;
                        ResponseModel.Messages = messages;
                    }
                    else
                    {
                        List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                        ResponseModel.ResultCode = "ERR";
                        ResponseModel.Messages = messages;
                    }

                    response = Ok(ResponseModel);
                }
                else
                {
                    response=DisplayMessage("Zone doesn't exist");
                    Log.Error("Failed to toggle Zone in ToggleZoneActivation(): " + zoneId + ", With flag: " + flag + " because zone does not exist"); 

                }

            }
            catch (Exception e)
            {
                Log.Error($"Failed to update the zone due to the following error: {e.Message}");
                Log.Error(e.StackTrace);
                List<CorporateAPI.Models.Response.Message> messages = new List<CorporateAPI.Models.Response.Message>() { new CorporateAPI.Models.Response.Message() { Code = "ARCA-E0403", Text = "Failed to process the request" } };
                Response.StatusCode = 400;
                ResponseModel.ResultCode = "ERR";
                ResponseModel.Messages = messages;
                response = BadRequest(ResponseModel);
            }
            return response;
        }
    }
}