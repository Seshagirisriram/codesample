﻿using log4net;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CorporateAPI
{

    /// <summary>
    /// Mongo Db Helper Class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MongoDBHelper<T> where T : class
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(MongoDBHelper<T>));
        /// <summary>
        /// Mongo DB Client Instance
        /// </summary>
        private MongoClient Client = null;

        /// <summary>
        /// MongoDB Database Instance
        /// </summary>
        private IMongoDatabase Database = null;

        /// <summary>
        /// MongoDb Collection Instance of type T <see cref="T"/>
        /// </summary>
        private IMongoCollection<T> Collection = null;

        private bool IsConnectionOpened = false;

        public MongoClient getClientFromConfig()
        {
            string adminDB = "admin"; //  System.Configuration.ConfigurationManager.AppSettings["MongoMasterDatabaseName"];
            string userName = "cicduser"; //  System.Configuration.ConfigurationManager.AppSettings["MongoUsername"];
            string password = "asDf%sak083Kjf"; //  System.Configuration.ConfigurationManager.AppSettings["MongoPassword"];
            string host = "13.235.135.219"; //  System.Configuration.ConfigurationManager.AppSettings["MongoHost"];
            string port = "47023"; //  System.Configuration.ConfigurationManager.AppSettings["MongoPort"];

            int mPort = 27017;
            if (!string.IsNullOrEmpty(port))
            {
                mPort = Convert.ToInt32(port);
            }
            Log.Debug("Mongo Server:" + host);
            Log.Debug("Mongo Port: " + mPort);
            MongoCredential credential = null;
            if (string.IsNullOrEmpty(adminDB) || string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
            {
                Log.Info("Performing anonymous login.");
                var settings1 = new MongoClientSettings
                {
                    Server = new MongoServerAddress(host, mPort),
                };
                return new MongoClient(settings1);
            }
            else
            {
                Log.Info($"Validating with user name/password on admin db: {adminDB}"); 
                credential = MongoCredential.CreateCredential(adminDB, userName, password);
                var settings2 = new MongoClientSettings
                {
                    Credentials = new[] { credential },
                    Server = new MongoServerAddress(host, mPort),
                };
                return new MongoClient(settings2);

            }

        }

        /// <summary>
        /// Establishing connection to the database
        /// </summary>
        /// <param name="ConnectionString">Connection String</param>
        /// <returns></returns>
        private void Open()
        {
            //Client = new MongoClient(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            if (Client == null)
            {
                Client = getClientFromConfig(); 
            }
            var clientDB = "cicdswarak"; //  System.Configuration.ConfigurationManager.AppSettings["MongoClientDatabaseName"];
            Database = Client.GetDatabase(clientDB);
            Collection = this.Database.GetCollection<T>("aividata");
            IsConnectionOpened = true;
        }

        // <summary>
        /// Establishing connection to the database and opening the collection
        /// </summary>
        /// <param name="ConnectionString">Connection String</param>
        /// <returns></returns>
        private void Open(String CollectionName)
        {
            //Client = new MongoClient(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            if (Client == null)
            {
                Client = getClientFromConfig();
            }
            var clientDB = "cicdswarak";  //System.Configuration.ConfigurationManager.AppSettings["MongoClientDatabaseName"];
            Database = Client.GetDatabase(clientDB); 
            Collection = this.Database.GetCollection<T>(CollectionName);
            IsConnectionOpened = true;
        }

        /// <summary>
        /// Execute the Query
        /// </summary>
        /// <param name="CollectionName"></param>
        /// <param name="filterDefinition"></param>
        /// <returns></returns>
        public List<T> Find(FilterDefinition<T> filterDefinition)
        {
            this.Open();
            return this.Collection.Find(filterDefinition).ToList<T>();
        }

        /// <summary>
        /// Execute the Query
        /// </summary>
        /// <param name="CollectionName"></param>
        /// <param name="filterDefinition"></param>
        /// <returns></returns>
        public List<T> Find(String CollectionName, Expression<Func<T, bool>> where)
        {
            this.Open(CollectionName);
            return this.Collection.Find(where).ToList<T>();
        }

        /// <summary>
        /// Find and Increment
        /// </summary>
        /// <param name="sequenceValue"></param>
        /// <returns></returns>
        public int FindAndIncrement(String sequenceValue) {
            if (Client == null)
            {
                Client = getClientFromConfig(); 
            }
            var clientDB = System.Configuration.ConfigurationManager.AppSettings["MongoClientDatabaseName"];
            Database = Client.GetDatabase(clientDB);
            var collection = Database.GetCollection<BsonDocument>("Counter");
            var filter = Builders<BsonDocument>.Filter.Eq(x => x["_id"], sequenceValue); ;
            var update = Builders<BsonDocument>.Update.Inc("sequence_value", 1);            
            BsonDocument Document=collection.FindOneAndUpdateAsync(filter, update).Result;
            BsonValue value= Document.GetValue("sequence_value");            
            return value.ToInt32();
        }

        /// <summary>
        /// Custom Search Conditions
        /// </summary>
        /// <param name="today"></param>
        /// <returns></returns>
        public List<T> FetchAiviDataDateSearch(DateTime startDate, DateTime endDate)
        {
            this.Open();

            var projectDefintion = Builders<BsonDocument>.Projection.Exclude("timeStampAsDate");
            var expression = new BsonDocument(new List<BsonElement>
            {
                new BsonElement("timeStampAsDate", new BsonDocument(new BsonElement("$toDate", "$TimeStamp")))
            });
            var addFieldStage = new BsonDocument(new BsonElement("$addFields", expression));
            var gteFilter =
                Builders<BsonDocument>.Filter.Gte(x => x["timeStampAsDate"], new BsonDateTime(startDate));
            var lteFilter =
                Builders<BsonDocument>.Filter.Lte(x => x["timeStampAsDate"], new BsonDateTime(endDate));
            var result = this.Collection.Aggregate().AppendStage<BsonDocument>(addFieldStage)
                .Match(gteFilter).Match(lteFilter).Project<BsonDocument>(projectDefintion).As<T>();

            var docs = result;
            return result.ToList<T>();
        }

        /// <summary>
        /// Insert the Records into the MongoDB
        /// </summary>
        /// <param name="SyncData">Data of Type <see cref="T"/> </param>
        /// <returns></returns>
        public async Task<bool> InsertRecords(T SyncData,String CollectionName)
        {
            try
            {
                this.Open(CollectionName);
                await this.Collection.InsertOneAsync(SyncData);
            }
            catch (Exception e) {
                Console.WriteLine(e);
            }
            
            return true;
        }

        /// <summary>
        /// Insert the Records into the MongoDB
        /// </summary>
        /// <param name="SyncData">Data of Type <see cref="T"/> </param>
        /// <returns></returns>
        public async Task<bool> UpdateRecords(T SyncData, String CollectionName, Expression<Func<T, bool>> where)
        {
            try
            {
                this.Open(CollectionName);
                await this.Collection.ReplaceOneAsync<T>(where,SyncData);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return true;
        }

    }
}
