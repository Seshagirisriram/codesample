﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using ImageResizingDrawing.Model;
using Newtonsoft.Json;
using System.Linq;
using System.Configuration;


namespace ImageResizingDrawing
{
    public class ZoneResizingDrawing
    {
        public void DrawZones(string JsonFile,string SourceFile)
        {
           // string jsonFilePath = ConfigurationManager.AppSettings["jsonPath"].ToString();
            string json = System.IO.File.ReadAllText(JsonFile);
            //string json = System.IO.File.ReadAllText(jsonFilePath);

            ZoneImageListModel jsonResponse = new ZoneImageListModel();
            jsonResponse = JsonConvert.DeserializeObject<ZoneImageListModel>(json);

            //To resize an image

            Image originalImage = Image.FromFile(SourceFile);

            string resolution = jsonResponse.annotation_resolution;
            string[] resolutionList = resolution.Split('x');

            int width = Convert.ToInt32(resolutionList[0]);
            int height = Convert.ToInt32(resolutionList[1]);
            Bitmap image = ResizeImage(originalImage, width, height);
            ////

            //To Draw ROI Polygon
            List<Point> polyPoints = new List<Point>();
            var responseROI = (from s in jsonResponse.zones
                               where s.name == "ROI"
                               select new
                               {
                                   s.coordinates,
                                   s.name

                               }).FirstOrDefault();

            string[] roiList = responseROI.coordinates.Split(',');
            string[] points = roiList[0].Split(':');
            float xpoint = float.Parse(points[0]);
            float ypoint = float.Parse(points[1]);
            PointF firstLocation = new PointF(xpoint, ypoint);
            polyPoints = new List<Point>();
            foreach (string x in roiList)
            {
                object[] roiSide = x.ToString().Split(":");
                int xcoordinate = Convert.ToInt32(roiSide[0]);
                int ycoordinate = Convert.ToInt32(roiSide[1]);
                polyPoints.Add(new Point(xcoordinate, ycoordinate));
            }
            string roiDrawnPath = ConfigurationManager.AppSettings["zoneImagePath"].ToString();
            Color customColorRed = Color.FromArgb(60, Color.Red);
            SolidBrush redBrush = new SolidBrush(customColorRed);
           // Bitmap resizedImage = new Bitmap(ConfigurationManager.AppSettings["resizeImagePath"].ToString());
            Graphics g = Graphics.FromImage(image);
            g.DrawPolygon(Pens.Red, polyPoints.ToArray());
            g.FillPolygon(redBrush, polyPoints.ToArray());
            g.DrawString(responseROI.name, new Font("Tahoma", 10), Brushes.White, firstLocation);
            g.Save();
            image.Save(roiDrawnPath);
            //

            //To draw zones inside the ROI

            var responseNotROI = (from s in jsonResponse.zones
                                  where s.name != "ROI"
                                  select new
                                  {
                                      s.coordinates,
                                      s.name

                                  }).ToList();

            foreach (var zone in responseNotROI)
            {
                string zoneCoordinates = zone.coordinates;
                string[] zoneList = zoneCoordinates.Split(',');

                string[] zonelistPoints = zoneList[0].Split(':');
                float xpoints = float.Parse(zonelistPoints[0]);
                float ypoints = float.Parse(zonelistPoints[1]);
                PointF polygonLocation = new PointF(xpoints, ypoints);

                polyPoints = new List<Point>();
                foreach (string x in zoneList)
                {
                    object[] dff = x.ToString().Split(":");
                    int xcoordinate = Convert.ToInt32(dff[0]);
                    int ycoordinate = Convert.ToInt32(dff[1]);
                    polyPoints.Add(new Point(xcoordinate, ycoordinate));
                }
                string polygonDrawnPath = ConfigurationManager.AppSettings["zoneImagePath"].ToString();

                Color customColorGreen = Color.FromArgb(120, Color.LimeGreen);
                SolidBrush greenBrush = new SolidBrush(customColorGreen);

                g.DrawPolygon(Pens.Green, polyPoints.ToArray());
                g.FillPolygon(greenBrush, polyPoints.ToArray());
                g.DrawString(zone.name, new Font("Tahoma", 10), Brushes.White, polygonLocation);
                g.Save();
                image.Save(polygonDrawnPath);
               
            }
            g.Dispose();
            image.Dispose();
        }
        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }
            string resizePath = ConfigurationManager.AppSettings["resizeImagePath"].ToString();
            destImage.Save(resizePath);
            return destImage;
        }
       }
}
