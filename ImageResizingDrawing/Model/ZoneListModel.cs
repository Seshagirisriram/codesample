﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ImageResizingDrawing.Model
{
    public class ZoneImageListModel
    {

        public string Id;
        public string camera_name;
        public string camera_ip;
        public string camera_port;
        public string camera_user;
        public string camera_password;
        public string camera_murl;
        public string annotation_resolution;
        public List<ZoneDetailsListResponseModel> zones { get; set; }
        }
    public class ZoneDetailsListResponseModel
    {
        public string name { get; set; }
        public string type { get; set; }
        public string shape { get; set; }
        public string coordinates { get; set; }
        public string mapping_rule { get; set; }
        public string version { get; set; }
        public string marking_datetime { get; set; }
    }

}

