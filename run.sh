for i in `find ./reports -name "*.html"`
do 
	b=`basename $i`
	if [ $b != "codeanalysis.html" ] 
	then
		awk -f strip_header_footer_info.awk $i | sed 's/<br \/>|//' > /tmp/f
		mv /tmp/f $i 
	fi
done
