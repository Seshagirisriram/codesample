#FROM mcr.microsoft.com/dotnet/core/sdk:3.1.401-bionic-arm64v8 as builder
FROM mcr.microsoft.com/dotnet/core/sdk:3.1.401-alpine3.12 as builder
#copy just the project file over
# this prevents additional extraneous restores
# and allows us to re-use the intermediate layer
# This only happens again if we change the csproj.
# This means WAY faster builds!
# would have loved this, but cannot do so because these are all tied together like mad !!!

COPY ./CorporateAPI /src/
WORKDIR /src
RUN dotnet restore /src/CorporateAPI.csproj
RUN dotnet publish -c release -o  /published  -r linux-musl-x64  /p:LinkDuringPublish=true /p:ShowLinkerSizeComparison=true /src/CorporateAPI.csproj
#RUN dotnet publish -c release -o  /published  -r linux-arm64  /p:LinkDuringPublish=true /p:ShowLinkerSizeComparison=true /src/FootfallApis/CorporateAPI.csproj
#
#
FROM mcr.microsoft.com/dotnet/core/runtime:3.1.7-alpine3.12
#FROM mcr.microsoft.com/dotnet/core/runtime:3.1.7-bionic-arm64v8
WORKDIR /published
COPY --from=builder /published .
ENV ASPNETCORE_URLS=http://+:5000
ENV SWARAK_INST_DIR=/db 
EXPOSE 8283/tcp
#
CMD ["/published/CorporateAPI"] 


